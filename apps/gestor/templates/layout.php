<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<?php include_http_metas() ?>
<?php include_metas() ?>

<?php include_title() ?>

<link rel="shortcut icon" href="/favicon.ico" />
<link rel="stylesheet" type="text/css" media="screen" href="/css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/sf/sf_admin/css/main.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/css/menu.css" />

<!--[if lt IE 7.]>
<script defer type="text/javascript" src="/js/pngfix.js"></script>
<![endif]-->

<script type="text/javascript">
  function idioma(lang) {
    new Ajax.Request('/lang/index?lang='+lang,{ method: 'post', onSuccess: doReload, onFailure: doError });
  }

  function doReload() {
    document.location.reload();
  }

  function doError() {
    alert(Messages.c);
  }
</script>

</head>
<body>

<div id="header">
  <div style="float: left">
    <div style="padding: 10px">
      <img src="/images/header1.png" width="300" height="80" />    
    </div>
  </div>
  
  <div style="float: right">
    <div style="padding: 5px">
      <img src="/images/header2.png" width="260" height="90" />
    </div>
  </div>
</div>

  <div id="menu">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="background-color: #000">
      <tr>
        <td width="100%">
          <div id="myMenu">
            <!-- MENU NUEVO -->
            <?php
            menulayout::draw();
            ?>
          </div>
        </td>
        <td style="background-color: #000">
          <div style="width: 75px;" align="center">
            <table border="0" style="background-color: #000">
              <tr>
                <td style="background-color: #000; padding: 2px; border: 1px solid #fff"><a href="javascript:void(0)" onclick="javascript:idioma('en_US')"><?php echo image_tag('/images/flags/uk.gif',array('alt'=>__('Ingles'))) ?></a></td>
                <td style="background-color: #000; padding: 2px; border: 1px solid #fff"><a href="javascript:void(0)" onclick="javascript:idioma('es_ES')"><?php echo image_tag('/images/flags/es.gif',array('alt'=>__('Espanol'))) ?></a></td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </table>
  </div>

<div id="sf_admin_container">
  <div id="sf_admin_content">
    <?php echo $sf_data->getRaw('sf_content') ?>
  </div>
</div>

<div align="right" style="border-top: 4px solid #0650A3; padding: 5px">&copy;<?php echo date('Y') ?> ferrandini.com</div>
</body>
</html>
