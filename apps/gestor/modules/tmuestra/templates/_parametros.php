<div id="parametros">
<?php
include_once('js/tmuestra.edit.php');

$c = new Criteria();
$c->add(ParametrosPeer::ID_TIPOMUESTRA, $tipomuestra->getPrimaryKey());
$pRS = ParametrosPeer::doSelectRS($c);

$id_fase = 0;
$id_separador = 0;

while ($pRS->next()) {
  $param = new Parametros();
  $param->hydrate($pRS);

  if($id_fase!=$param->getIdFase() && $param->getIdFase()>0) {
    if($id_fase>0) {
            ?>
            </table>
            <br>
            <?php
    }
    $id_fase = $param->getIdFase();
    $id_separador = 0;
    ?>
    <div id="f[<?php echo $param->getPrimarykey() ?>]" style="position: relative">
      <h2><?php echo $param->getFases()->getNombre() ?></h2>
      <div id="fa[<?php echo $param->getPrimaryKey() ?>]" style="position: absolute; top: 2px; right: 2px">
        <a href="javascript:void(0)" onclick="javascript:deleteFase(<?php echo $param->getPrimaryKey(); ?>)">
          <?php echo image_tag('/sf/sf_admin/images/delete.png',array('alt' => __('delete'))); ?>
        </a>
      </div>
    </div>
    <?php
  }

  if($param->getIdSeparador()!=$id_separador && $param->getIdSeparador()>0) {
    $id_separador = $param->getIdSeparador();
    ?>
    <div id="s[<?php echo $param->getPrimarykey() ?>]" style="position: relative; padding: 5px; border-bottom: 2px solid #000" align="center">
      <b><?php echo $param->getSeparadores()->getNombre() ?></h2></b>
      <div id="sa[<?php echo $param->getPrimaryKey() ?>]" style="position: absolute; top: 2px; right: 2px">
        <a href="javascript:void(0)" onclick="javascript:deleteSeparador(<?php echo $param->getPrimaryKey(); ?>)">
          <?php echo image_tag('/sf/sf_admin/images/delete.png',array('alt' => __('delete'))); ?>
        </a>
      </div>
    </div>
    <?php
  }
  ?>
  <div id="p[<?php echo $param->getPrimarykey() ?>]" class="parametro" style="position: relative; padding: 3px; <?php echo $param->getNombre()=='Descriptor'?'font-weight: bold; ':'' ?>border-bottom: <?php echo $param->getNombre()=='Descriptor'?'2':'1' ?>px solid #000">
    <div id="pn[<?php echo $param->getPrimarykey() ?>]" style="padding: 5px"><?php echo $param->getNombre() ?></div>
    <?php if($param->getNombre()!='Descriptor'): ?>
    <div id="pe[<?php echo $param->getPrimarykey() ?>]" style="display: none; padding: 1px"></div>
    <div id="pa[<?php echo $param->getPrimaryKey() ?>]" style="position: absolute; top: 6px; right: 3px">
      <a href="javascript:void(0)" onclick="javascript:setParametroSeparador(<?php echo $param->getPrimaryKey(); ?>)">
        <?php echo image_tag('/sf/sf_admin/images/separator.png',array('alt' => __('separator'))); ?>
      </a>
      <a href="javascript:void(0)" onclick="javascript:editParametro(<?php echo $param->getPrimaryKey(); ?>)">
        <?php echo image_tag('/sf/sf_admin/images/edit.png',array('alt' => __('edit'))); ?>
      </a>
      <a href="javascript:void(0)" onclick="javascript:deleteParametro(<?php echo $param->getPrimaryKey(); ?>)">
        <?php echo image_tag('/sf/sf_admin/images/delete.png',array('alt' => __('delete'))); ?>
      </a>
    </div>
    <div id="pea[<?php echo $param->getPrimaryKey() ?>]" style="position: absolute; display: none; top: 6px; right: 3px">
      <a href="javascript:void(0)" onclick="javascript:saveEditParametro(<?php echo $param->getPrimaryKey() ?>)"><img src="/sf/sf_admin/images/save.png" /></a>
      <a href="javascript:void(0)" onclick="javascript:cancelEditParametro(<?php echo $param->getPrimaryKey(); ?>)">
        <?php echo image_tag('/sf/sf_admin/images/cancel.png',array('alt' => __('cancel'))); ?>
      </a>
    </div>
    <?php endif; ?>
  </div>
  <?php
}
?>
</div>

&nbsp;
<div id="p[n]" style="position: relative; padding: 3px" align="center">
  <div style="float: left; padding: 5px">
    <table border="0" cellpadding="0" cellspacing="0" class="sf_admin_list" style="width:">
      <tr>
        <th colspan="2"><?php echo __('Nueva fase') ?></th>
      </tr>
      <tr>
        <td style="background-color: #444; border: 1px solid #444">
          <label for="p_f" style="color: #fff"><?php echo __('Fase') ?>:</label>
          <?php echo input_tag('p[f]', '', array('size' => '40')); ?>
        </td>
        <td style="background-color: #444; border: 1px solid #444">
          <ul class="sf_admin_actions">
            <li><input class="sf_admin_action_save" value="Guardar" type="button" onclick="javascript:addFase()" /></li>
          </ul>
        </td>
      </tr>
    </table>
  </div>

  <div style="float: left; padding: 5px">
    <table border="0" cellpadding="0" cellspacing="0" class="sf_admin_list" style="width:">
      <tr>
        <th colspan="2"><?php echo __('Nuevo separador') ?></th>
      </tr>
      <tr>
        <td style="background-color: #444; border: 1px solid #444">
          <label for="p_s" style="color: #fff"><?php echo __('Separador') ?>:</label>
          <?php echo input_hidden_tag('p[s][p]','') ?>
          <?php echo input_tag('p[s]', '', array('size' => '40')); ?>
        </td>
        <td style="background-color: #444; border: 1px solid #444">
          <ul class="sf_admin_actions">
            <li><input class="sf_admin_action_save" value="Guardar" type="button" onclick="javascript:addSeparador()" /></li>
          </ul>
        </td>
      </tr>
    </table>
  </div>

  <div style="float: left; padding: 5px">
    <table border="0" cellpadding="0" cellspacing="0" class="sf_admin_list" style="width:">
      <tr>
        <th colspan="2"><?php echo __('Nuevo parametro') ?></th>
      </tr>
      <tr>
        <td style="background-color: #444; border: 1px solid #444">
          <label for="p_n" style="color: #fff"><?php echo __('Parametro') ?>:</label>
          <?php echo input_tag('p[n]', '', array('size' => '40')); ?>
        </td>
        <td style="background-color: #444; border: 1px solid #444">
          <ul class="sf_admin_actions">
            <li><input class="sf_admin_action_save" value="Guardar" type="button" onclick="javascript:addParametro()" /></li>
          </ul>
        </td>
      </tr>
    </table>
  </div>

</div>
