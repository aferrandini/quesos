<?php

/**
 * tmuestra actions.
 *
 * @package    quesos
 * @subpackage tmuestra
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class tmuestraActions extends autotmuestraActions
{
  public function executeClean() {
    $this->tipomuestra = $this->getTipomuestraOrCreate();

    if($this->tipomuestra->getPrimaryKey()>0) {
      $c = new Criteria();
      $c->addJoin(MuestrasPeer::ID_MUESTRA, ResultadosPeer::ID_MUESTRA);
      $c->add(MuestrasPeer::ID_TIPO, $this->tipomuestra->getPrimaryKey());
      ResultadosPeer::doDelete($c);

      $c = new Criteria();
      $c->add(MuestrasPeer::ID_TIPO, $this->tipomuestra->getPrimaryKey());
      MuestrasPeer::doDelete($c);
      
      for ($i=0;$i<5;$i++) {
        $muestra = new Muestras();
        $muestra->setIdTipo($this->tipomuestra->getPrimaryKey());
        $muestra->setNombre($this->tipomuestra->getNombre());
        $muestra->save();
      }
      
      $this->setFlash('notice', 'Los datos se han eliminado correctamente.');
    } else {
      $this->getRequest()->setError('delete', 'No se han podido eliminar los datos correctamente.');

      return $this->forward('tmuestra', 'list');
    }
    return $this->redirect('tmuestra/list');
  }
}
