<?php

/**
 * lang actions.
 *
 * @package    localizagps
 * @subpackage lang
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 1139 2006-04-07 08:46:23Z fabien $
 */
class langActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executeIndex()
  {
    if($this->getRequestParameter('lang')!='') {
      $this->getUser()->setCulture($this->getRequestParameter('lang'));
    }
  }
}

?>