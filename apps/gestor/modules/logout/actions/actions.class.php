<?php

/**
 * logout actions.
 *
 * @package    aridos
 * @subpackage logout
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 1139 2006-04-07 08:46:23Z fabien $
 */
class logoutActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executeIndex()
  {
    $user=$this->getUser();
    $user->clearCredentials();
    $user->setAuthenticated(false);
    session_destroy();
    $this->forward('default','Index');
  }
}

?>