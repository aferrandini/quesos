<div id="f[<?php echo $param->getPrimarykey() ?>]" style="position: relative">
  <h2><?php echo $param->getFases()->getNombre() ?></h2>
  <div id="fa[<?php echo $param->getPrimaryKey() ?>]" style="position: absolute; top: 2px; right: 2px">
    <a href="javascript:void(0)" onclick="javascript:deleteFase(<?php echo $param->getPrimaryKey(); ?>)">
      <?php echo image_tag('/sf/sf_admin/images/delete.png',array('alt' => __('delete'))); ?>
    </a>
  </div>
</div>

<div id="p[<?php echo $param->getPrimarykey() ?>]" class="parametro" style="position: relative; padding: 3px; font-weight: bold; border-bottom: 2px solid #000">
  <div id="pn[<?php echo $param->getPrimarykey() ?>]" style="padding: 5px"><?php echo $param->getNombre() ?></div>
</div>
