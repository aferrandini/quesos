<?php
if($nuevo) {
  ?>
  <div id="p[<?php echo $param->getPrimarykey() ?>]" class="parametro" style="position: relative; padding: 3px; border-bottom: 1px solid #000">
    <div id="pn[<?php echo $param->getPrimarykey() ?>]" style="padding: 5px"><?php echo $param->getNombre() ?></div>
    <div id="pe[<?php echo $param->getPrimarykey() ?>]" style="display: none; padding: 1px"></div>
    <div id="pa[<?php echo $param->getPrimaryKey() ?>]" style="position: absolute; top: 6px; right: 3px">
      <a href="javascript:void(0)" onclick="javascript:setParametroSeparador(<?php echo $param->getPrimaryKey(); ?>)">
        <?php echo image_tag('/sf/sf_admin/images/separator.png',array('alt' => __('separator'))); ?>
      </a>
      <a href="javascript:void(0)" onclick="javascript:editParametro(<?php echo $param->getPrimaryKey(); ?>)">
        <?php echo image_tag('/sf/sf_admin/images/edit.png',array('alt' => __('edit'))); ?>
      </a>
      <a href="javascript:void(0)" onclick="javascript:deleteParametro(<?php echo $param->getPrimaryKey(); ?>)">
        <?php echo image_tag('/sf/sf_admin/images/delete.png',array('alt' => __('delete'))); ?>
      </a>
    </div>
    <div id="pea[<?php echo $param->getPrimaryKey() ?>]" style="position: absolute; display: none; top: 6px; right: 3px">
      <a href="javascript:void(0)" onclick="javascript:saveEditParametro(<?php echo $param->getPrimaryKey() ?>)"><img src="/sf/sf_admin/images/save.png" /></a>
      <a href="javascript:void(0)" onclick="javascript:cancelEditParametro(<?php echo $param->getPrimaryKey(); ?>)">
        <?php echo image_tag('/sf/sf_admin/images/cancel.png',array('alt' => __('cancel'))); ?>
      </a>
    </div>
  </div>
  <?php
} else {
  echo $param->getNombre();
}
