<?php

/**
 * ajax actions.
 *
 * @package    quesos
 * @subpackage ajax
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class ajaxActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executeSaveeditparametro()
  {
    if($this->getRequestParameter('p')!='') {
      $this->nuevo = false;
      $this->param = ParametrosPeer::retrieveByPK($this->getRequestParameter('p'));
    }  elseif($this->getRequestParameter('t')!='') {
      $this->nuevo = true;
      $this->param = new Parametros();
      $this->param->setIdTipomuestra($this->getRequestParameter('t'));
    }
    $this->param->setNombre($this->getRequestParameter('v'));
    $this->param->save();
  }

  public function executeDeleteseparador() {
    $this->param = ParametrosPeer::retrieveByPK($this->getRequestParameter('p'));
    $this->param->setIdSeparador(null);
    $this->param->save();
  }
  
  public function executeDeleteparametro() {
    $this->param = ParametrosPeer::retrieveByPK($this->getRequestParameter('p'));
    $this->param->delete();
  }

  public function executeNewfase() {
    /**
     * t: id_tipomuestra
     * f: nombre fase
     */
    if($this->getRequestParameter('t')!='') {
      $c = new Criteria();
      $c->add(FasesPeer::NOMBRE , $this->getRequestParameter('f'));
      $fase = FasesPeer::doSelectOne($c);
      if(!($fase instanceof Fases )) {
        $fase = new Fases();
        $fase->setNombre($this->getRequestParameter('f'));
        $fase->save();
      }

      $this->param = new Parametros();
      $this->param->setIdTipomuestra($this->getRequestParameter('t'));
      $this->param->setNombre('Descriptor');
      $this->param->setIdFase($fase->getPrimaryKey());
      $this->param->save();
    }
  }

  public function executeNewseparador() {
    /**
     * t: id_tipomuestra
     * s: nombre separador
     */
    if($this->getRequestParameter('s')!='') {
      $c = new Criteria();
      $c->add(SeparadoresPeer::NOMBRE , $this->getRequestParameter('s'));
      $separador = SeparadoresPeer::doSelectOne($c);
      if(!($separador instanceof Separadores )) {
        $separador = new Separadores();
        $separador->setNombre($this->getRequestParameter('s'));
        $separador->save();
      }

      $this->param = ParametrosPeer::retrieveByPk($this->getRequestParameter('p'));
      /*
      $c = new Criteria();
      $c->add(ParametrosPeer::ID_TIPOMUESTRA, $this->getRequestParameter('t'));
      $c->adDescendingOrderByColumn(ParametrosPeer::ID_PARAMETRO );
      $this->param = ParametrosPeer::doSlectOne($c);
      */
      $this->param->setIdSeparador($separador->getPrimaryKey());
      $this->param->save();
    }
  }

}
