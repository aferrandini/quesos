<h1>Resultados ofrecidos por los jueces</h1>
<ul class="sf_admin_actions">
  <li><input class="sf_admin_action_edit" value="Editar resultados" type="button" onclick="document.location.href='/jueces?mod=1';" /></li>
</ul>
<div align="center">
<?php
$id_muestra = 0;
$id_fase = 0;
$id_separador = 0;

while ($pRS->next()) {
	$param = new Parametros();
	$param->hydrate($pRS);
	
	if($param->getIdMuestra()!=$id_muestra) {
	  if($id_muestra>0) {
	    ?>
       </table>
       <br><br>
       <?php
	  }
	  $id_muestra = $param->getIdMuestra();
	  $id_fase = 0;
	  $id_separador = 0;
	  ?>
	  <h1><?php echo $param->getMuestras()->getNombre() ?></h1>
    <?php
	}
	
	if($id_fase!=$param->getIdFase()) {
	  if($id_fase>0) {
	    ?>
	    </table>
	    <br>
	    <?php
	  }
	  $id_fase = $param->getIdFase();
	  $id_separador = 0;
	  ?>
	  <h2><?php echo $param->getFases()->getNombre() ?></h2>
	  <table cellspacing="0" class="sf_admin_list" style="width: 520px">
	  <?php
	}
	
	if($param->getIdSeparador() && $param->getIdSeparador()!=$id_separador) {
	  $id_separador = $param->getIdSeparador();
	  ?>
	  <tr>
	   <td colspan="11" align="center"><b><?php echo $param->getSeparadores()->getNombre() ?></b></td>
	  </tr>
	  <?php
	}
	
	?>
	<tr>
	 <td style="width: 400px; <?php echo $param->getNombre()=='Descriptor'?'border-bottom: 2px solid #000':'' ?>"><?php echo $param->getNombre()=='Descriptor'?'<b>'.$param->getNombre().'</b>':$param->getNombre() ?></td>
	 <?php 
	 if (isset($res[$param->getPrimaryKey()])) {
	   $val = $res[$param->getPrimaryKey()];
	 } else {
	   $val = 'NULL';
	 }
	 for ($i=0;$i<10;$i++) {
	   ?>
	   <td style="width: 40px; background: #<?php echo ($i%2?'ccc':'fff') ?>; ; <?php echo $param->getNombre()=='Descriptor'?'border-bottom: 2px solid #000':'' ?>" align="center">
	     <?php echo ($i==$val?radiobutton_tag($param->getPrimaryKey(),'x',true):'') ?>
	     <br/>
	     <?php echo $i ?>
     </td>
     <?php
	 }
	 ?>
  </tr>
  <?php
}
?>
</table>
</div>

<ul class="sf_admin_actions">
  <li><input class="sf_admin_action_edit" value="Editar resultados" type="button" onclick="document.location.href='/jueces?mod=1';" /></li>
</ul>