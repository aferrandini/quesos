<h1><?php echo __('Agregar resultados de los jueces') ?></h1>
<div align="center" style="padding: 20px">
  <table cellspacing="0" class="sf_admin_list" style="width: 700px">
    <tr>
      <th width="100%"><b><?php echo __('Muestras') ?></b></th>
      <th><b><?php echo __('Acciones') ?></b></th>
    </tr>
    <?php 
    $total = 0;
    $validas = 0;
    $muestras_totales = MuestrasPeer::doCount(new Criteria());
    
    $c = new Criteria();
    $c->addAscendingOrderByColumn(MuestrasPeer::ID_TIPO);
    $c->addAscendingOrderByColumn(MuestrasPeer::ID_MUESTRA);
    foreach (MuestrasPeer::doSelect($c) as $muestra): ?>
    <tr>
      <td><b><?php echo $muestra->getNombre() ?></b></th>
      <td align="center">
        <?php
        $c = new Criteria();
        $c->add(ParametrosPeer::ID_TIPOMUESTRA , $muestra->getIdTipo());
        $parametros = ParametrosPeer::doCount($c);
        
        $c = new Criteria();
        $c->add(ResultadojuezPeer::ID_MUESTRA, $muestra->getPrimaryKey());
        $respuestas = ResultadojuezPeer::doCount($c);
        
        if($respuestas<$parametros) {
          // ponemos el edit
          echo link_to(image_tag('/sf/sf_admin/images/edit.png',array()),'jueces/add?id_muestra='.$muestra->getPrimaryKey(),array());
        } else {
          // ponemos el ok
          $validas++;
          echo image_tag('/sf/sf_admin/images/ok.png',array());
          if ($sf_user->getAttribute('admin')) {
            // ponemos el admin
            echo link_to(image_tag('/sf/sf_admin/images/edit.png',array()),'jueces/add?id_muestra='.$muestra->getPrimaryKey(),array());
          }
        }
        ?>
      </td>
    </tr>
    <?php endforeach; ?>
    <tr>
      <th><?php echo __('Total') ?>:</th>
      <th style="text-align: center"><?php echo $validas.'/'.$muestras_totales ?></th>
    </tr>
  </table>
</div>