<?php include_once 'js/resultados.php'; ?>

<h1><?php echo __('Agregar resultados de los jueces') ?></h1>
<?php echo form_tag('/jueces/saveres',array('method'=>'post')) ?>
<?php echo input_hidden_tag('id_muestra',$muestra->getPrimaryKey()) ?>

<h1><?php echo $muestra->getNombre() ?></h1>

<div align="center">
<?php
$id_fase = 0;
$id_separador = 0;

$p = 0;
while ($pRS->next()) {
	$param = new Parametros();
	$param->hydrate($pRS);
	
	if($id_fase!=$param->getIdFase() && $param->getIdFase()>0) {
    if($id_fase>0) {
	    ?>
	    </table>
	    <br>
	    <?php
	  }
	  $id_fase = $param->getIdFase();
	  $id_separador = 0;
	  ?>
	  <h2><?php echo $param->getFases()->getNombre() ?></h2>
	  <table cellspacing="0" class="sf_admin_list" style="width: 520px">
	  <?php
	}
	
	if($param->getIdSeparador()!=$id_separador && $param->getIdSeparador()>0) {
    $id_separador = $param->getIdSeparador();
    ?>
	  <tr>
	   <td colspan="11" align="center"><b><?php echo $param->getSeparadores()->getNombre() ?></b></td>
	  </tr>
	  <?php
	}
	
	?>
	<tr>
	 <td style="width: 400px; <?php echo $param->getNombre()=='Descriptor'?'border-bottom: 2px solid #000':'' ?>"><?php echo $param->getNombre()=='Descriptor'?'<b>'.$param->getNombre().'</b>':$param->getNombre() ?></td>
	 <?php 
	 if (isset($res[$param->getPrimaryKey()])) {
	   $val = $res[$param->getPrimaryKey()];
	 } else {
	   $val = 5;
	 }
	 for ($i=1;$i<10;$i++) {
	   ?>
	   <td style="width: 40px; background: #<?php echo ($i%2?'ccc':'fff') ?>; ; <?php echo $param->getNombre()=='Descriptor'?'border-bottom: 2px solid #000':'' ?>" align="center">
	     <?php echo radiobutton_tag('param['.$param->getPrimaryKey().']',$i,$i==$val?true:false,array('id' => 'param['.$p.']['.$i.']', 'onchange' => 'javascript:checkParam('.$p.','.$i.')')); ?>
	     <br/>  
	     <?php //echo radiobutton_tag('param['.$param->getPrimaryKey().']',$i,$i==$val?true:false); ?>
	     <a href="javascript:void(0)" onclick="javascript:checkParam(<?php echo $p ?>,<?php echo $i ?>)"><?php echo $i ?></a>
     </td>
     <?php
	 }
	 ?>
  </tr>
  <?php
  $p++;
}
?>
</table>

<ul class="sf_admin_actions">
  <li><input class="sf_admin_action_cancel" value="Cancelar" type="button" onclick="document.location.href='/jueces';" /></li>
  <li><input type="submit" name="save" value="Guardar y Listado" class="sf_admin_action_save" /></li>
</ul>
</div>
