<?php

/**
 * jueces actions.
 *
 * @package    quesos
 * @subpackage jueces
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class juecesActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executeIndex() {

  }

  public function executeAdd()
  {
    $this->muestra = MuestrasPeer::retrieveByPK($this->getRequestParameter('id_muestra'));
    
    $this->res = array();
    $c = new Criteria();
    $c->add(ResultadojuezPeer::ID_MUESTRA , $this->muestra->getPrimaryKey());
    
    $rRS = ResultadojuezPeer::doSelectRS($c);
    while ($rRS->next()) {
      $jRes = new Resultadojuez();
      $jRes->hydrate($rRS);

      $this->res[$jRes->getIdParametro()] = $jRes->getResultado();
    }
    
    $c = new Criteria();
    $c->addJoin(MuestrasPeer::ID_TIPO, ParametrosPeer::ID_TIPOMUESTRA );
    $c->add(MuestrasPeer::ID_MUESTRA , $this->muestra->getPrimaryKey());
    
    $this->pRS = ParametrosPeer::doSelectRS($c);
  }

  public function executeResultados() {
    $con = Propel::getConnection();
    $sql = "SELECT id_parametro, resultado FROM resultadojuez ORDER BY id_parametro";
    $stmt = $con->prepareStatement($sql);
    $jRes = $stmt->executeQuery();
    $resultadosJueces = array();
    while ($jRes->next()) {
      $id_param = $jRes->get('id_parametro');
      $resultad = $jRes->get('resultado');
      $resultadosJueces[$id_param] = $resultad;
    }
    $this->res = $resultadosJueces;
    $this->pRS = ParametrosPeer::doSelectRS(new Criteria());
  }

  public function executeSaveres() {
    $id_muestra = $this->getRequestParameter('id_muestra');
    
    $con = Propel::getConnection();
    $sql = "DELETE FROM resultadojuez WHERE id_muestra='$id_muestra'";
    $stmt = $con->prepareStatement($sql);
    $stmt->executeQuery();

    $parametros = $this->getRequestParameter('param');
    $sql = "INSERT INTO resultadojuez (id_muestra,id_parametro,resultado) VALUES ";
    foreach ($parametros as $id_param => $jRes) {
      $sql .= "('$id_muestra','$id_param','$jRes'), ";
    }
    $sql = substr($sql,0,strlen($sql)-2);
    $stmt = $con->prepareStatement($sql);
    $stmt->executeQuery();

    return $this->redirect('/jueces');
  }
}
