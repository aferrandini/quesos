<?php

/**
 * login actions.
 *
 * @package    aridos
 * @subpackage login
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 1139 2006-04-07 08:46:23Z fabien $
 */
class loginActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executeIndex()
  {
      return sfView::SUCCESS;
  }
  
  public function executeLogin()
  {
  	$user=$this->getUser();

    $c=new Criteria();
    $c->add(UsuariosPeer::LOGIN  ,$this->getRequestParameter('usuario'));
    $usuario = UsuariosPeer::doSelectOne($c);

    if (empty($usuario)){
      //nadie tiene ese login
      $user->setAuthenticated(false);
      return $this->redirect('/login');
    }

    if (strcmp($usuario->getPass(),md5($this->getRequestParameter('password')))==0) {
      $user->clearCredentials();
      // remember that the current user is authenticated
      $user->setAuthenticated(true);
      //a�ador credenciales
      //$group=$usuario->getLevel();
      /*foreach($group as $join) {
        $user->addCredential($join->getPrivilegio()->getNombre());
      }*/
      //guardarlo en el usuario
      $user->setAttribute('id_usuario',$usuario->getidusuario()); 
      if($usuario->getAdmin()) {
        $user->setAttribute('admin',true);
        $user->addCredential('admin');
      } else {
        $user->setAttribute('admin',false);
      }
      
      $user->setCulture('es_ES');
      return $this->redirect('/');
    } else {
      // not authenticated
      $user->setAuthenticated(false);
      return $this->redirect('/login');
    }
  }
}

?>