<?php echo form_tag('login/login') ?>

<div style="padding: 20px" align="center">
  <table border="0" cellpadding="0" cellspacing="0" style="border: 3px solid #2A8EFE" align="center">
    <tr>
      <td><?php echo __('Usuario') ?>:</td>
      <td><?php echo input_tag('usuario', $sf_params->get('usuario')) ?></td>
    </tr>
    <tr>
      <td><?php echo __('Pass') ?>:</td>
      <td><?php echo input_password_tag('password') ?></td>
    </tr>
    
    <tr>
      <td colspan="2" align="right">
        <ul class="sf_admin_actions">
          <li><?php echo submit_tag(__('Entrar'),array('class'=>"sf_admin_action_security" )) ?></li>
        </ul>
      </td>
    </tr>
  </table>
</div>
</form>
