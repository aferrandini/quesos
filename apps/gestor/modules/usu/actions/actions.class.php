<?php

/**
 * usu actions.
 *
 * @package    quesos
 * @subpackage usu
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class usuActions extends autousuActions
{
  protected function updateUsuariosFromRequest()
  {
    $usuarios = $this->getRequestParameter('usuarios');

    if (isset($usuarios['nombre']))
    {
      $this->usuarios->setNombre($usuarios['nombre']);
    }
    if (isset($usuarios['login']))
    {
      $this->usuarios->setLogin($usuarios['login']);
    }
    if (isset($usuarios['passo']) && isset($usuarios['passv']) && $usuarios['passo']!=='' && $usuarios['passo']===$usuarios['passv'])
    {
      $this->usuarios->setPass(md5($usuarios['passo']));
    }
    if (isset($usuarios['admin']) && $usuarios['admin']>0)
    {
      $this->usuarios->setAdmin(1);
    } else {
      $this->usuarios->setAdmin(0);
    }
  }
}
