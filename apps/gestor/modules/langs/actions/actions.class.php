<?php

/**
 * langs actions.
 *
 * @package    localizagps
 * @subpackage langs
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class langsActions extends autolangsActions
{
  public function executeChange() {
    $this->idiomas = $this->getIdiomasOrCreate();

    $c = new Criteria();
    $c->clearSelectColumns()->addSelectColumn(IdiomatraduccionPeer::ID_TRADUCCION );
    $c->add(IdiomatraduccionPeer::ID_IDIOMA, $this->idiomas->getPrimaryKey());
    $tiRS = IdiomatraduccionPeer::doSelectRS($c);

    $trads = array();
    while ($tiRS->next()) {
      $trads[] = $tiRS->get(1);
    }

    $c = new Criteria();
    $c->add(TraduccionesPeer::ID_TRADUCCION , $trads, Criteria::IN );
    $c->addAscendingOrderByColumn(TraduccionesPeer::TEXTO);
    $this->tsRS = TraduccionesPeer::doSelectRS($c);

    $c = new Criteria();
    $c->add(TraduccionesPeer::ID_TRADUCCION , $trads, Criteria::NOT_IN );
    $c->addAscendingOrderByColumn(TraduccionesPeer::TEXTO);
    $this->tnRS = TraduccionesPeer::doSelectRS($c);
  }

  public function executeEdit() {
    return $this->executeList();
  }

  public function executeDelete() {
    return $this->executeList();
  }

  public function executeSave() {
    $this->idiomas = $this->getIdiomasOrCreate();
    if($this->idiomas instanceof Idiomas) {
      $trans = $this->getRequestParameter('trans');
      $toDelete = array();
      foreach ($trans as $id_trad => $trad) {
        if(trim($trad)!='') {
          $c = new Criteria();
          $c->add(IdiomatraduccionPeer::ID_IDIOMA, $this->idiomas->getPrimaryKey());
          $c->add(IdiomatraduccionPeer::ID_TRADUCCION, $id_trad);
          $idiomatrad = IdiomatraduccionPeer::doSelectOne($c);
          if(!($idiomatrad instanceof Idiomatraduccion)) {
            $idiomatrad = new Idiomatraduccion();
            $idiomatrad->setIdIdioma($this->idiomas->getPrimaryKey());
            $idiomatrad->setIdTraduccion($id_trad);
          }
          $idiomatrad->setTexto(trim($trad));
          $idiomatrad->save();
        } else {
          $toDelete[] = $id_trad;
        }
      }
      if(count($toDelete)>0) {
        $c = new Criteria();
        $c->add(IdiomatraduccionPeer::ID_IDIOMA, $this->idiomas->getPrimaryKey());
        $c->add(IdiomatraduccionPeer::ID_TRADUCCION, $toDelete, Criteria::IN );
        IdiomatraduccionPeer::doDelete($c);
      }

      $place = "0";
      foreach ($this->getRequestParameter('save') as $place => $save);

      return $this->redirect('/langs/change?id_idioma='.$this->idiomas->getPrimaryKey().($place>0?'#'.$place:''));
    } else {
      return $this->executeList();
    }
  }

}
