<div id="sf_admin_content">
  <h1><?php echo __('Idioma') ?>: <?php echo $idiomas->getNombre() ?></h1>
  <?php echo form_tag('langs/save', array('method'=>'post')) ?>
  <?php echo input_hidden_tag('id_idioma',$idiomas->getPrimaryKey()) ?>
  <table border="0" cellpadding="0" cellspacing="0" class="sf_admin_list">
    <?php
    $i = 1;
    while ($tnRS->next()) {
      $traduccion = new Traducciones();
      $traduccion->hydrate($tnRS);

      $idiomatraduccion = new Idiomatraduccion();
      ?>

      <tr class="sf_admin_row_<?php echo ($i%2) ?>">
        <td width="50%" valign="top"><div id="text[<?php echo $traduccion->getPrimaryKey() ?>]"><?php echo $traduccion->getTexto() ?></div></td>
        <td><a href="javascript:void(0)" onclick="$('trans_<?php echo $traduccion->getPrimaryKey() ?>').value=$('text[<?php echo $traduccion->getPrimaryKey() ?>]').innerHTML;">&raquo;</a></td>
        <td valign="top" width="50%">
        <?php echo textarea_tag('trans['.$traduccion->getPrimaryKey().']',$idiomatraduccion->getTexto(),array('style'=>'width: 98%; height: 50px')) ?>
        </td>
      </tr>
      <?php
      if ($i%10==0) {
      	?>
      	<tr>
          <td colspan="3" align="right">
            <a name="<?php echo intval($i/10) ?>"></a>
            <ul class="sf_admin_actions">
              <li><input name="save[<?php echo intval($i/10) ?>]" value="<?php echo __('save') ?>" class="sf_admin_action_save" type="submit"></li>
              <li><input class="sf_admin_action_list" value="<?php echo __('list') ?>" onclick="document.location.href='/langs/list';" type="button"></li>
            </ul>
          </td>
      	</tr>
      	<?php
      }
      $i++;
    }

    while ($tsRS->next()) {
      $traduccion = new Traducciones();
      $traduccion->hydrate($tsRS);

      $c = new Criteria();
      $c->add(IdiomatraduccionPeer::ID_IDIOMA, $idiomas->getPrimaryKey());
      $c->add(IdiomatraduccionPeer::ID_TRADUCCION, $traduccion->getPrimaryKey());
      $idiomatraduccion = IdiomatraduccionPeer::doSelectOne($c);
      if(!($idiomatraduccion instanceof Idiomatraduccion)) {
        $idiomatraduccion = new Idiomatraduccion();
      }
      ?>

      <tr class="sf_admin_row_<?php echo ($i%2) ?>">
        <td width="50%" valign="top"><div id="text[<?php echo $traduccion->getPrimaryKey() ?>]"><?php echo $traduccion->getTexto() ?></div></td>
        <td><a href="javascript:void(0)" onclick="$('trans_<?php echo $traduccion->getPrimaryKey() ?>').value=$('text[<?php echo $traduccion->getPrimaryKey() ?>]').innerHTML;">&raquo;</a></td>
        <td valign="top" width="50%">
        <?php echo textarea_tag('trans['.$traduccion->getPrimaryKey().']',$idiomatraduccion->getTexto(),array('style'=>'width: 98%; height: 50px')) ?>
        </td>
      </tr>
      <?php
      if ($i%10==0) {
      	?>
      	<tr>
          <td colspan="3" align="right">
            <a name="<?php echo intval($i/10) ?>"></a>
            <ul class="sf_admin_actions">
              <li><input name="save[<?php echo intval($i/10) ?>]" value="<?php echo __('save') ?>" class="sf_admin_action_save" type="submit"></li>
              <li><input class="sf_admin_action_list" value="<?php echo __('list') ?>" onclick="document.location.href='/langs/list';" type="button"></li>
            </ul>
          </td>
      	</tr>
      	<?php
      }
      $i++;
    }
    ?>
  </table>
</div>