<?php

/**
 * alu actions.
 *
 * @package    quesos
 * @subpackage alu
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class aluActions extends autoaluActions
{
  public function executeMuestras() {
    $this->alumnos = $this->getAlumnosOrCreate();
  }
  
  public function executeAdd() {
    $this->alumnos = $this->getAlumnosOrCreate();
    $this->muestra = MuestrasPeer::retrieveByPK($this->getRequestParameter('id_muestra'));
    
    $this->res = array();
    $c = new Criteria();
    $c->add(ResultadosPeer::ID_ALUMNO , $this->alumnos->getPrimaryKey());
    $c->add(ResultadosPeer::ID_MUESTRA , $this->muestra->getPrimaryKey());
    
    $rRS = ResultadosPeer::doSelectRS($c);
    while ($rRS->next()) {
      $uRes = new Resultados();
      $uRes->hydrate($rRS);

      $this->res[$uRes->getIdParametro()] = array($uRes->getRespuesta(),$uRes->getPunish());
    }

    $c = new Criteria();
    $c->addJoin(MuestrasPeer::ID_TIPO, ParametrosPeer::ID_TIPOMUESTRA );
    $c->add(MuestrasPeer::ID_MUESTRA , $this->muestra->getPrimaryKey());
    
    $this->pRS = ParametrosPeer::doSelectRS($c);
  }

  public function executeRes() {
    $this->alumnos = $this->getAlumnosOrCreate();
    $id_alumno = $this->alumnos->getPrimaryKey();

    $con = Propel::getConnection();
    $sql = "SELECT id_parametro, respuesta, punish FROM resultados WHERE id_alumno='$id_alumno'";
    $stmt = $con->prepareStatement($sql);
    $res = $stmt->executeQuery();
    $usuarioRes = array();
    while($res->next()) {
      $usuarioRes[$res->get('id_parametro')] = array($res->get('respuesta'),$res->get('punish'));
    }
    $this->res = $usuarioRes;

    $con = Propel::getConnection();
    $sql = "SELECT id_parametro, resultado FROM resultadojuez ORDER BY id_parametro;";
    $stmt = $con->prepareStatement($sql);
    $res = $stmt->executeQuery();
    $juecesRes = array();
    while ($res->next()) {
    	$juecesRes[$res->get('id_parametro')] = $res->get('resultado');
    }
    $this->jRes = $juecesRes;
    
    $this->pRS = ParametrosPeer::doSelectRS(new Criteria());
  }

  public function executeSaveres() {
    $this->alumnos = $this->getAlumnosOrCreate();

    $id_alumno = $this->alumnos->getPrimaryKey();
    $id_muestra = $this->getRequestParameter('id_muestra');
    
    $con = Propel::getConnection();
    $sql = "DELETE FROM resultados WHERE id_alumno='$id_alumno' and id_muestra='$id_muestra'";
    $stmt = $con->prepareStatement($sql);
    $stmt->executeQuery();

    $parametros = $this->getRequestParameter('param');
    $punish = $this->getRequestParameter('punish');
    
    $sql = "INSERT INTO resultados (id_alumno,id_muestra,id_parametro,respuesta,punish) VALUES ";
    foreach ($parametros as $id_param => $uRes) {
      $sql .= "('$id_alumno','$id_muestra','$id_param','$uRes','".((isset($punish[$id_param]) && $punish[$id_param]>0)?'1':'0')."'), ";
    }
    $sql = substr($sql,0,strlen($sql)-2);
    $stmt = $con->prepareStatement($sql);
    $stmt->executeQuery();

    return $this->redirect('/alu/muestras?id_alumno='.$id_alumno);
  }

  public function executeCrearinsert() {
    $this->pRS = ParametrosPeer::doSelectRS(new Criteria());
  }

  public function executeResultados() {
    $this->pdf = new pdflistadoparticipantes();

    $response = $this->getContext()->getResponse();
    $response->setHttpHeader('Pragma', '');
    $response->setHttpHeader('Cache-Control', '');
    $response->setHttpHeader('Content-Type', 'application/pdf');
    $response->setHttpHeader('Content-Disposition', 'attachment; filename="listado_resultados.pdf"');
  }
}
