<h1><?php echo __('Resultados') ?></h1>
<ul class="sf_admin_actions">
  <li><input class="sf_admin_action_list" value="<?php echo __('Listado') ?>" type="button" onclick="document.location.href='/alu/list';" /></li>
</ul>
<div align="center">
  <table cellspacing="0" class="sf_admin_list" style="width: 700px">
    <tr>
      <th style="border-bottom: 1px solid #fff" width="30%"><b><?php echo __('Alumno') ?>:</b></th>
      <td><?php echo $alumnos->getNombre() ?></td>
    </tr>
    
    <tr>
      <th style="border-bottom: 1px solid #fff"><b><?php echo __('Equipo') ?>:</b></th>
      <td><?php echo $alumnos->getEquipos()->getNombre() ?></td>
    </tr>
    
    <tr>
      <th style="border-bottom: 1px solid #fff"><b><?php echo __('Profesor') ?>:</b></th>
      <td><?php echo $alumnos->getEquipos()->getProfesor() ?></td>
    </tr>
    
    <tr>
      <th style="border-bottom: 1px solid #fff"><b><?php echo __('Pais') ?>:</b></th>
      <td><?php echo $alumnos->getEquipos()->getPaises()->getFlag().' '.$alumnos->getEquipos()->getPaises()->getPais() ?></td>
    </tr>
  </table>
</div>

<div align="center" style="padding: 20px">
  <table cellspacing="0" class="sf_admin_list" style="width: 700px">
    <tr>
      <th width="100%"><b><?php echo __('Muestras') ?></b></th>
      <th><b><?php echo __('Resultado') ?></b></th>
    </tr>
    <?php 
    $total = 0;
    $total_tipo = 0;
    $validas = 0;
    $muestras_totales = MuestrasPeer::doCount(new Criteria());
    $id_tipo = 0;
    $nombre_muestra = '';

    $c = new Criteria();
    $c->addAscendingOrderByColumn(MuestrasPeer::ID_TIPO);
    $c->addAscendingOrderByColumn(MuestrasPeer::ID_MUESTRA);
    foreach (MuestrasPeer::doSelect($c) as $muestra): 
      if($id_tipo!=$muestra->getIdTipo()) {
        if($id_tipo>0) {
          ?>
          <tr>
            <th width="100%"><b><?php echo __('Total de la muestra de %%muestra%%',array('%%muestra%%'=>$nombre_muestra)) ?></b></th>
            <th style="text-align: right"><?php echo $total_tipo ?></th>
          </tr>
          <?php
        }
        $nombre_muestra = $muestra->getTipomuestra()->getNombre();
        $c = new Criteria();
        $c->add(ParametrosPeer::ID_TIPOMUESTRA , $muestra->getIdTipo());
        $parametros = ParametrosPeer::doCount($c);
        $id_tipo = $muestra->getIdTipo();
    
        $total_tipo = 0;
      }
    ?>
    <tr>
      <td><b><?php echo $muestra->getNombre() ?></b></th>
      <td align="right">
        <?php 
        $resultado = $muestra->getResultado($alumnos->getPrimaryKey());
        $total_tipo += $resultado;
        $total += $resultado;
        echo $resultado;
       ?>
      </td>
    </tr>
    <?php endforeach; ?>
    <tr>
      <th width="100%"><b><?php echo __('Total de la muestra de %%muestra%%',array('%%muestra%%'=>$nombre_muestra)) ?></b></th>
      <th style="text-align: right"><?php echo $total_tipo ?></th>
    </tr>
    <tr>
      <td colspan=2>&nbsp;</td>
    </tr>
    <tr>
      <th><?php echo __('Total') ?>:</th>
      <th style="text-align: right"><?php echo $total ?></th>
    </tr>
    
  </table>
</div>
<ul class="sf_admin_actions">
  <li><input class="sf_admin_action_list" value="<?php echo __('Listado') ?>" type="button" onclick="document.location.href='/alu/list';" /></li>
</ul>
