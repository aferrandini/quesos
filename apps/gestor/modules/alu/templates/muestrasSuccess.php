<h1><?php echo __('Agregar resultados') ?></h1>
<ul class="sf_admin_actions">
  <li><input class="sf_admin_action_list" value="<?php echo __('Listado') ?>" type="button" onclick="document.location.href='/alu/list';" /></li>
</ul>
<div align="center">
  <table cellspacing="0" class="sf_admin_list" style="width: 700px">
    <tr>
      <th style="border-bottom: 1px solid #fff" width="30%"><b><?php echo __('Alumno') ?>:</b></th>
      <td><?php echo $alumnos->getNombre() ?></td>
    </tr>
    
    <tr>
      <th style="border-bottom: 1px solid #fff"><b><?php echo __('Equipo') ?>:</b></th>
      <td><?php echo $alumnos->getEquipos()->getNombre() ?></td>
    </tr>
    
    <tr>
      <th style="border-bottom: 1px solid #fff"><b><?php echo __('Profesor') ?>:</b></th>
      <td><?php echo $alumnos->getEquipos()->getProfesor() ?></td>
    </tr>
    
    <tr>
      <th style="border-bottom: 1px solid #fff"><b><?php echo __('Pais') ?>:</b></th>
      <td><?php echo $alumnos->getEquipos()->getPaises()->getFlag().' '.$alumnos->getEquipos()->getPaises()->getPais() ?></td>
    </tr>
  </table>
</div>

<div align="center" style="padding: 20px">
  <table cellspacing="0" class="sf_admin_list" style="width: 700px">
    <tr>
      <th width="100%"><b><?php echo __('Muestras') ?></b></th>
      <th><b><?php echo __('Acciones') ?></b></th>
      <th><b><?php echo __('Resultado') ?></b></th>
    </tr>
    <?php 
    $total = 0;
    $validas = 0;
    $muestras_totales = MuestrasPeer::doCount(new Criteria());
    $id_tipo = 0;

    $c = new Criteria();
    $c->addAscendingOrderByColumn(MuestrasPeer::ID_TIPO);
    $c->addAscendingOrderByColumn(MuestrasPeer::ID_MUESTRA);
    foreach (MuestrasPeer::doSelect($c) as $muestra): 
      if($id_tipo!=$muestra->getIdTipo()) {
        $c = new Criteria();
        $c->add(ParametrosPeer::ID_TIPOMUESTRA , $muestra->getIdTipo());
        $parametros = ParametrosPeer::doCount($c);
        $id_tipo = $muestra->getIdTipo();
      }
    ?>
    <tr>
      <td><b><?php echo $muestra->getNombre() ?></b></th>
      <td align="center">
        <?php
        $c = new Criteria();
        $c->add(ResultadosPeer::ID_ALUMNO , $alumnos->getPrimaryKey());
        $c->add(ResultadosPeer::ID_MUESTRA, $muestra->getPrimaryKey());
        $respuestas = ResultadosPeer::doCount($c);
        
        if($respuestas<$parametros) {
          // ponemos el edit
          echo link_to(image_tag('/sf/sf_admin/images/edit.png',array()),'alu/add?id_alumno='.$alumnos->getPrimaryKey().'&id_muestra='.$muestra->getPrimaryKey(),array());
        } else {
          // ponemos el ok
          $validas++;
          echo image_tag('/sf/sf_admin/images/ok.png',array());
          if ($sf_user->getAttribute('admin')) {
            // ponemos el admin
            echo link_to(image_tag('/sf/sf_admin/images/edit.png',array()),'alu/add?id_alumno='.$alumnos->getPrimaryKey().'&id_muestra='.$muestra->getPrimaryKey(),array());
          }
        }
        ?>
      </td>
      <td align="right">
        <?php 
        $resultado = $muestra->getResultado($alumnos->getPrimaryKey());
        $total += $resultado;
        echo $resultado;
       ?>
      </td>
    </tr>
    <?php endforeach; ?>
    <tr>
      <th><?php echo __('Total') ?>:</th>
      <th style="text-align: center"><?php echo $validas.'/'.$muestras_totales ?></th>
      <th style="text-align: right"><?php echo $total ?></th>
    </tr>
    
  </table>
</div>
<ul class="sf_admin_actions">
  <li><input class="sf_admin_action_list" value="<?php echo __('Listado') ?>" type="button" onclick="document.location.href='/alu/list';" /></li>
</ul>
