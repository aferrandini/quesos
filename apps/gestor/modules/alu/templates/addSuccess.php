<?php include_once 'js/resultados.php'; ?>

<h1><?php echo __('Agregar resultados') ?></h1>
<ul class="sf_admin_actions">
  <li><input class="sf_admin_action_cancel" value="Cancelar" type="button" onclick="document.location.href='/alu/muestras/id_alumno/<?php echo $alumnos->getPrimaryKey() ?>';" /></li>
</ul>
<div align="center">
  <table cellspacing="0" class="sf_admin_list" style="width: 700px">
    <tr>
      <th style="border-bottom: 1px solid #fff" width="30%"><b><?php echo __('Alumno') ?>:</b></th>
      <td><?php echo $alumnos->getNombre() ?></td>
    </tr>
    
    <tr>
      <th style="border-bottom: 1px solid #fff"><b><?php echo __('Equipo') ?>:</b></th>
      <td><?php echo $alumnos->getEquipos()->getNombre() ?></td>
    </tr>
    
    <tr>
      <th style="border-bottom: 1px solid #fff"><b><?php echo __('Profesor') ?>:</b></th>
      <td><?php echo $alumnos->getEquipos()->getProfesor() ?></td>
    </tr>
    
    <tr>
      <th style="border-bottom: 1px solid #fff"><b><?php echo __('Pais') ?>:</b></th>
      <td><?php echo $alumnos->getEquipos()->getPaises()->getFlag().' '.$alumnos->getEquipos()->getPaises()->getPais() ?></td>
    </tr>
    
    <tr>
      <th valign="top"><b><?php echo __('Muestra') ?>:</b></th>
      <td><?php echo $muestra->getNombre() ?></td>
    </tr>
  </table>
</div>

<?php echo form_tag('/alu/saveres',array('method'=>'post')) ?>
<?php echo input_hidden_tag('id_alumno',$alumnos->getPrimaryKey()) ?>
<?php echo input_hidden_tag('id_muestra',$muestra->getPrimaryKey()) ?>
<div align="center">
<?php
$id_fase = 0;
$id_separador = 0;

$p = 0;
while ($pRS->next()) {
	$param = new Parametros();
	$param->hydrate($pRS);
	
	if($id_fase!=$param->getIdFase() && $param->getIdFase()>0) {
    if($id_fase>0) {
            ?>
            </table>
            <br>
            <?php
    }
    $id_fase = $param->getIdFase();
    $id_separador = 0;
	  ?>
	  <h2><?php echo $param->getFases()->getNombre() ?></h2>
	  <table cellspacing="0" cellpadding="0" class="sf_admin_list" style="width: 520px">
	  <?php
	}
	
	if($param->getIdSeparador()!=$id_separador && $param->getIdSeparador()>0) {
    $id_separador = $param->getIdSeparador();
    ?>
	  <tr>
	   <td colspan="12" align="center"><b><?php echo $param->getSeparadores()->getNombre() ?></b></td>
	  </tr>
	  <?php
	}
	
	?>
	<tr>
	 <td style="width: 400px; border-bottom: <?php echo $param->getNombre()=='Descriptor'?'2':'1' ?>px solid #000"><?php echo $param->getNombre()=='Descriptor'?'<b>'.$param->getNombre().'</b>':$param->getNombre() ?></td>
	<?php
    if(!isset($res[$param->getPrimaryKey()])) {
	   $res[$param->getPrimaryKey()] = array(5,false);
	 } 
	 echo input_hidden_tag('param['.$param->getPrimaryKey().']',$res[$param->getPrimaryKey()][0],array('id'=>'param_'.$p));
 
	 for ($i=1;$i<10;$i++) {
	   ?>
	   <td onclick="javascript:checkParam(<?php echo $p ?>,<?php echo $i ?>)" style="width: 40px; background: #<?php echo ($i%2?'ccc':'fff') ?>; border-bottom: <?php echo $param->getNombre()=='Descriptor'?'2':'1' ?>px solid #000" align="center">
	     <?php echo radiobutton_tag('radio_param['.$param->getPrimaryKey().']',$i,($i==$res[$param->getPrimaryKey()][0]?true:false),array('id' => 'param['.$p.']['.$i.']', 'onchange' => 'javascript:checkParam('.$p.','.$i.')', 'disabled' => ($res[$param->getPrimaryKey()][1]?true:false))); ?>
	     <br/>
	     <a href="javascript:void(0)" onclick="javascript:checkParam(<?php echo $p ?>,<?php echo $i ?>)"><?php echo $i ?></a>
     </td>
     <?php
	 }
	 ?>
	 <td style="width: 40px; background: #fcc; border-bottom: <?php echo $param->getNombre()=='Descriptor'?'2':'1' ?>px solid #000" align="center">
     <?php echo checkbox_tag('punish['.$param->getPrimaryKey().']',1,($res[$param->getPrimaryKey()][1]?true:false), array('id'=>'punish['.$p.']','onchange'=>'javascript:checkPunish('.$p.')')); ?>
   </td>
  </tr>
  <?php
  
  $p++;
}
?>
</table>

<ul class="sf_admin_actions">
  <li><input class="sf_admin_action_cancel" value="Cancelar" type="button" onclick="document.location.href='/alu/muestras/id_alumno/<?php echo $alumnos->getPrimaryKey() ?>';" /></li>
  <li><input type="submit" name="save" value="Guardar" class="sf_admin_action_save" /></li>
</ul>
</div>