<?php

class translations {
  static function create($culture='') {
    if($culture!='') {
      $c = new Criteria();
      $c->add(IdiomasPeer::CULTURE, $culture);
      $idioma = IdiomasPeer::doSelectOne($c);

      if(!($idioma instanceof Idiomas)) {
        // el culture no tiene idioma
        $culture = 'en_US';
        //ParametrosPeer::retrieveByPK(6)->getValor();
        if($culture=='' || strlen($culture)<=2) $culture = 'en_US';

        $c = new Criteria();
        $c->add(IdiomasPeer::CULTURE, $culture);
        $idioma = IdiomasPeer::doSelectOne($c);
      }
    } else {
      // No nos han dado culture
      $culture = 'en_US';
      //ParametrosPeer::retrieveByPK(6)->getValor();
      if($culture=='' || strlen($culture)<=2) $culture = 'en_US';

      $c = new Criteria();
      $c->add(IdiomasPeer::CULTURE, $culture);
      $idioma = IdiomasPeer::doSelectOne($c);
    }

    $con = Propel::getConnection();
    $stmt = $con->createStatement();
    $sql = "SELECT t.texto, it.texto FROM traducciones t, idiomatraduccion it WHERE it.id_idioma='".$idioma->getPrimaryKey()."' AND it.id_traduccion=t.id_traduccion";
    $rs = $stmt->executeQuery($sql, ResultSet::FETCHMODE_NUM );

    $trans = array();
    while ($rs->next()) {
      $trans[$rs->get(1)] = $rs->get(2);
    }

    return $trans;
  }
}
