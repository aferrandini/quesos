<script type="text/javascript">
var param = 0;

function checkParam(id,num) {
  if($('param['+id+']['+num+']') && $('param['+id+']['+num+']').disabled==false) {
    $('param['+id+']['+num+']').checked = true;
	if($('param_'+id)) $('param_'+id).value = num;
  }

  selectNext(id);
}

function checkPunish(id) {
  var setDisabledTo = false;
  if($('punish['+id+']').checked) setDisabledTo = true;
  
  for(var num=1;num<=9;num++) {
    $('param['+id+']['+num+']').disabled = setDisabledTo;
  }

  checkParam(id,5);
}

function selectPrevious(id) {
  if(id>=0 && $('param['+(param-1)+'][1]')) {
  	param = id-1;

    if($('param['+param+'][1]')) {
		if($('param['+param+'][1]').disabled==false) {
          $('param['+param+'][1]').focus();
    	} else {
    	  $('punish['+param+']').focus();
    	}
    }
  }
}

function selectNext(id) {
  if(id>=0 && $('param['+(param+1)+'][1]')) {
  	param = id+1;

    if($('param['+param+'][1]')) {
		if($('param['+param+'][1]').disabled==false) {
          $('param['+param+'][1]').focus();
    	} else {
    	  $('punish['+param+']').focus();
    	}
    }
  }
}

function keypress(key) {
  switch(key) {
    case 49: //1
      checkParam(param,1);
      break;
    case 50: //2
   	  checkParam(param,2);
      break;
    case 51: //3
      checkParam(param,3);
      break;
    case 52: //4
    	checkParam(param,4);
      break;
    case 53: //5
    	checkParam(param,5);
      break;
    case 54: //6
    	checkParam(param,6);
      break;
    case 55: //7
    	checkParam(param,7);
      break;
    case 56: //8
    	checkParam(param,8);
      break;  
    case 57: //9
    	checkParam(param,9);
      break;
      
    case 80: //P
    case 112: //P
      if($('punish['+param+']').checked==true) {
    	$('punish['+param+']').checked = false;
    	checkPunish(param);
    	param = param-1;
      } else {
        $('punish['+param+']').checked = true;
        checkPunish(param);
      }
      break;

    case 78:  //N
    case 110: //N
      selectNext(param);
	  break;

    case 66: //B
    case 98: //B
      selectPrevious(param);
	  break;
  }
}

Event.observe(window, 'keypress', function(event){ var key = event.which || event.keyCode; keypress(key); });
</script>