<script type="text/javascript">
function deleteSeparador(id) {
  var params = { p: id };
  new Ajax.Request('/ajax/deleteseparador', {
    asynchronous: true,
    evalScripts: false,
    method: 'post',
    parameters: params,
    onLoading:function(request,json) {},
    onComplete:function(request,json) {
      if(parseInt(request.responseText)>0) {
        var elem = $('s['+id+']');
        if(elem) {
          elem.parentNode.removeChild(elem);
        }
      }
    }
  });
}

function deleteFase(id) {
  var params = { p: id };
  new Ajax.Request('/ajax/deleteparametro' , {
    asynchronous: true,
    evalScripts: false,
    method: 'post',
    parameters: params,
    onLoading:function(request,json) {},
    onComplete:function(request,json) {
      if(parseInt(request.responseText)>0) {
        var elem = $('f['+id+']');
        if(elem) {
          elem.parentNode.removeChild(elem);
        }
        var elem = $('p['+id+']');
        if(elem) {
          elem.parentNode.removeChild(elem);
        }
      }
    }
  });
}

function deleteParametro(id) {
  var params = { p: id };
  new Ajax.Request('/ajax/deleteparametro' , {
    asynchronous: true,
    evalScripts: false,
    method: 'post',
    parameters: params,
    onLoading:function(request,json) {},
    onComplete:function(request,json) {
      if(parseInt(request.responseText)>0) {
        var elem = $('p['+id+']');
        if(elem) {
          elem.parentNode.removeChild(elem);
        }
      }
    }
  });
}

function editParametro(id) {
  if($('edit')) {
    cancelEditParametro($('edit').parentNode.id.slice(3,-1));
  }

  $('p['+id+']').style.background = '#444444';
  Element.hide('pn['+id+']');
  Element.hide('pa['+id+']');
  $('pe['+id+']').innerHTML = '<input type="text" name="edit" id="edit" value="'+$('pn['+id+']').innerHTML+'" style="width: 95%" />';
  Element.show('pe['+id+']');
  Element.show('pea['+id+']');

  $('edit').focus();
}

function cancelEditParametro(id) {
  $('p['+id+']').style.background = '';
  Element.hide('pe['+id+']');
  Element.hide('pea['+id+']');
  $('pe['+id+']').innerHTML = '';
  Element.show('pn['+id+']');
  Element.show('pa['+id+']');
}

function saveEditParametro(id) {
  if($('edit') && $F('edit')!='') {
    var params = { p: id, v: $F('edit') };
    $('edit').innerHTML = '<img src="/images/waiting2.gif" />';
    Element.hide('pea['+id+']');
    new Ajax.Updater('pn['+id+']', '/ajax/saveeditparametro' , {
      asynchronous: true,
      evalScripts: false,
      method: 'post',
      parameters: params,
      onLoading:function(request,json) {},
      onComplete:function(request,json) {
        cancelEditParametro(id);
      }
    });
  }
}

function addParametro() {
  if($('p_n').value.length>0) {
    var params = { t: $F('id_tipomuestra'), v: $F('p_n') };
    new Ajax.Request('/ajax/saveeditparametro' , {
      asynchronous: true,
      evalScripts: false,
      method: 'post',
      parameters: params,
      onLoading:function(request,json) {},
      onComplete:function(request,json) {
        $('parametros').innerHTML += request.responseText;
        $('p_n').value = '';
        $('p_n').focus();
      }
    });
  } else {
    alert('Introduzca el nombre del parametro');
  }
}

function setParametroSeparador(id) {
  $('p_s_p').value = id;
  $('p_s').focus();
}
function addSeparador() {
  if($F('p_s')!='' && $F('p_s_p')>0) {
    var params = { t: $F('id_tipomuestra'), s: $F('p_s'), p: $F('p_s_p') };
    new Ajax.Request('/ajax/newseparador' , {
      asynchronous: true,
      evalScripts: false,
      method: 'post',
      parameters: params,
      onLoading:function(request,json) {},
      onComplete:function(request,json) {
        window.location.reload();
      }
    });
  } else {
    alert('Seleccione un parametro');
  }
}

function addFase() {
  if($F('p_f')!='') {
    var params = { t: $F('id_tipomuestra'), f: $F('p_f') };
    new Ajax.Request('/ajax/newfase' , {
      asynchronous: true,
      evalScripts: false,
      method: 'post',
      parameters: params,
      onLoading:function(request,json) {},
      onComplete:function(request,json) {
        $('parametros').innerHTML += request.responseText;
        $('p_f').value = '';
        $('p_n').focus();
      }
    });
  } else {
    alert('Introduzca el nombre de la fase');
  }
}
</script>
