<?php
/**
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SizeHelper returns a "( size unit )" text.
 *
 * @package    symfony
 * @subpackage helper
 * @author     Ariel Ferrandini
 * @version    SVN: $Id: TagHelper.php 765 2006-02-08 01:42:44Z fabien $
 */

/**
 *
 * @param  $size    int
 * @param  $unit    string (initial units)
 * @return string
 */

function size($size,$unit="b") {
  $array[0] = $size;
  $array[1] = $unit;

  $array = setUnits($array);

  return "( ".$array[0]." ".($array[1]=="b"?"":$array[1])."Bytes )";
}

function setUnits($size)
{
  if($size[1]!="G") {
    if($size[0]>=1024) {
      $size[0] = round($size[0]/1024,2);

      switch ($size[1]) {
      	case "b":
      		$size[1] = "K";
      		break;

        case "K":
          $size[1] = "M";
      		break;

        case "M":
          $size[1] = "G";
      		break;

      	default:
      	  $size[1] = "b";
      		break;
      }
      $size = setUnits($size);
    }
  }

  return $size;
}

?>