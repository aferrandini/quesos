<?php

class menulayout {

  static function draw() {
    ?>
    <div class="menuBar" style="width: 100%;">
        <a class="menuButton" href="/" onclick="javascript:void(0)"><?php echo __('Inicio'); ?></a>
        <?php  if(sfContext::getInstance()->getUser()->isAuthenticated()): ?>
        <a class="menuButton" href="/alu" onclick="javascript:void(0)"><?php echo __('Alumnos'); ?></a>
        <a class="menuButton" href="/team" onclick="javascript:void(0)"><?php echo __('Equipos'); ?></a>
        <?php if(sfContext::getInstance()->getUser()->hasCredential('admin')): ?>
        <a class="menuButton" href="/jueces" onclick="javascript:void(0)"><?php echo __('Jueces'); ?></a>
        <a class="menuButton" href="/usu" onclick="javascript:void(0)"><?php echo __('Usuarios'); ?></a>
        <a class="menuButton" href="/tmuestra" onclick="javascript:void(0)"><?php echo __('Muestras'); ?></a>
        <?php /*<a class="menuButton" href="/fases" onclick="javascript:void(0)"><?php echo __('Fases'); ?></a>*/ ?>
        <?php endif; ?>
        <a class="menuButton" href="/logout" onclick="javascript:void(0)"><?php echo __('Salir'); ?></a>
        <?php endif; ?>
    </div>
    <?php
  }
}
