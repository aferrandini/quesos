<?php

/**
 * Subclass for representing a row from the 'tipomuestra' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Tipomuestra extends BaseTipomuestra
{
  public function __toString() {
    return $this->getNombre();
  } 
}
