<?php



class JuecesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.JuecesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('jueces');
		$tMap->setPhpName('Jueces');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_JUEZ', 'IdJuez', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, false, 250);

	} 
} 