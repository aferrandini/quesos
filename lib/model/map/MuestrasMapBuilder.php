<?php



class MuestrasMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.MuestrasMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('muestras');
		$tMap->setPhpName('Muestras');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_MUESTRA', 'IdMuestra', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addForeignKey('ID_TIPO', 'IdTipo', 'int', CreoleTypes::INTEGER, 'tipomuestra', 'ID_TIPOMUESTRA', false, null);

	} 
} 