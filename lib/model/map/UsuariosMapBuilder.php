<?php



class UsuariosMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.UsuariosMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('usuarios');
		$tMap->setPhpName('Usuarios');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_USUARIO', 'IdUsuario', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('LOGIN', 'Login', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('PASS', 'Pass', 'string', CreoleTypes::VARCHAR, false, 32);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ADMIN', 'Admin', 'int', CreoleTypes::TINYINT, false, null);

	} 
} 