<?php



class TipomuestraMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TipomuestraMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('tipomuestra');
		$tMap->setPhpName('Tipomuestra');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_TIPOMUESTRA', 'IdTipomuestra', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, false, 100);

	} 
} 