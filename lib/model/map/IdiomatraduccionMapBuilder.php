<?php



class IdiomatraduccionMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.IdiomatraduccionMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('idiomatraduccion');
		$tMap->setPhpName('Idiomatraduccion');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_IDIOMATRADUCCION', 'IdIdiomatraduccion', 'string', CreoleTypes::BIGINT, true, null);

		$tMap->addForeignKey('ID_IDIOMA', 'IdIdioma', 'int', CreoleTypes::INTEGER, 'idiomas', 'ID_IDIOMA', false, null);

		$tMap->addForeignKey('ID_TRADUCCION', 'IdTraduccion', 'string', CreoleTypes::BIGINT, 'traducciones', 'ID_TRADUCCION', false, null);

		$tMap->addColumn('TEXTO', 'Texto', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 