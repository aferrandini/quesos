<?php



class ResultadosMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ResultadosMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('resultados');
		$tMap->setPhpName('Resultados');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_RESULTADO', 'IdResultado', 'string', CreoleTypes::BIGINT, true, null);

		$tMap->addForeignKey('ID_ALUMNO', 'IdAlumno', 'int', CreoleTypes::INTEGER, 'alumnos', 'ID_ALUMNO', false, null);

		$tMap->addForeignKey('ID_MUESTRA', 'IdMuestra', 'int', CreoleTypes::INTEGER, 'muestras', 'ID_MUESTRA', false, null);

		$tMap->addForeignKey('ID_PARAMETRO', 'IdParametro', 'int', CreoleTypes::INTEGER, 'parametros', 'ID_PARAMETRO', false, null);

		$tMap->addColumn('RESPUESTA', 'Respuesta', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('PUNISH', 'Punish', 'int', CreoleTypes::TINYINT, false, null);

	} 
} 