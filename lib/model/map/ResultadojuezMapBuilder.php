<?php



class ResultadojuezMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ResultadojuezMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('resultadojuez');
		$tMap->setPhpName('Resultadojuez');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_RESULTADOJUEZ', 'IdResultadojuez', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('ID_JUEZ', 'IdJuez', 'int', CreoleTypes::INTEGER, 'jueces', 'ID_JUEZ', false, null);

		$tMap->addForeignKey('ID_MUESTRA', 'IdMuestra', 'int', CreoleTypes::INTEGER, 'muestras', 'ID_MUESTRA', false, null);

		$tMap->addForeignKey('ID_PARAMETRO', 'IdParametro', 'int', CreoleTypes::INTEGER, 'parametros', 'ID_PARAMETRO', false, null);

		$tMap->addColumn('RESULTADO', 'Resultado', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 