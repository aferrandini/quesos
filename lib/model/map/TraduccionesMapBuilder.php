<?php



class TraduccionesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.TraduccionesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('traducciones');
		$tMap->setPhpName('Traducciones');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_TRADUCCION', 'IdTraduccion', 'string', CreoleTypes::BIGINT, true, null);

		$tMap->addColumn('TEXTO', 'Texto', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 