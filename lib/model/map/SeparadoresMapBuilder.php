<?php



class SeparadoresMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.SeparadoresMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('separadores');
		$tMap->setPhpName('Separadores');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_SEPARADOR', 'IdSeparador', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, true, 100);

	} 
} 