<?php



class FasesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.FasesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('fases');
		$tMap->setPhpName('Fases');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_FASE', 'IdFase', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 