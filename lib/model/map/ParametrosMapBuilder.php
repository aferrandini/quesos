<?php



class ParametrosMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ParametrosMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('parametros');
		$tMap->setPhpName('Parametros');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_PARAMETRO', 'IdParametro', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('ID_TIPOMUESTRA', 'IdTipomuestra', 'int', CreoleTypes::INTEGER, 'tipomuestra', 'ID_TIPOMUESTRA', false, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addForeignKey('ID_FASE', 'IdFase', 'int', CreoleTypes::INTEGER, 'fases', 'ID_FASE', false, null);

		$tMap->addForeignKey('ID_SEPARADOR', 'IdSeparador', 'int', CreoleTypes::INTEGER, 'separadores', 'ID_SEPARADOR', false, null);

	} 
} 