<?php



class PaisesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.PaisesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('paises');
		$tMap->setPhpName('Paises');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_PAIS', 'IdPais', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('PAIS', 'Pais', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('CODIGO', 'Codigo', 'string', CreoleTypes::VARCHAR, false, 2);

	} 
} 