<?php



class AlumnosMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.AlumnosMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('alumnos');
		$tMap->setPhpName('Alumnos');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_ALUMNO', 'IdAlumno', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addForeignKey('ID_EQUIPO', 'IdEquipo', 'int', CreoleTypes::INTEGER, 'equipos', 'ID_EQUIPO', false, null);

	} 
} 