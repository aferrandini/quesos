<?php



class IdiomasMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.IdiomasMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('idiomas');
		$tMap->setPhpName('Idiomas');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_IDIOMA', 'IdIdioma', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CULTURE', 'Culture', 'string', CreoleTypes::VARCHAR, true, 5);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, false, 100);

	} 
} 