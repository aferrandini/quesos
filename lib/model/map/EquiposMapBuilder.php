<?php



class EquiposMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.EquiposMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('equipos');
		$tMap->setPhpName('Equipos');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_EQUIPO', 'IdEquipo', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('PROFESOR', 'Profesor', 'string', CreoleTypes::VARCHAR, false, 150);

		$tMap->addForeignKey('ID_PAIS', 'IdPais', 'int', CreoleTypes::INTEGER, 'paises', 'ID_PAIS', false, null);

	} 
} 