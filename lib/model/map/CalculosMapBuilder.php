<?php



class CalculosMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.CalculosMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('calculos');
		$tMap->setPhpName('Calculos');

		$tMap->setUseIdGenerator(true);

		$tMap->addColumn('ID_TIPO', 'IdTipo', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ID_MUESTRA', 'IdMuestra', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ID_PARAMETRO', 'IdParametro', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ID_ALUMNO', 'IdAlumno', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('RESULTADO', 'Resultado', 'string', CreoleTypes::BIGINT, false, null);

		$tMap->addColumn('ID_EQUIPO', 'IdEquipo', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 