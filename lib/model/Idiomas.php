<?php

/**
 * Subclass for representing a row from the 'idiomas' table.
 *
 *
 *
 * @package lib.model
 */
class Idiomas extends BaseIdiomas
{
  public function getNtraducciones() {
    $total = TraduccionesPeer::doCount(new Criteria());
    $traducciones = $this->countIdiomatraduccions();
    return '<div align="right">'.round(($traducciones*100)/$total,2).'%</div>';
  }
}
