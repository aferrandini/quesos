<?php

/**
 * Subclass for representing a row from the 'equipos' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Equipos extends BaseEquipos
{
  public function __toString() {
    return $this->getNombre().' ['.$this->getProfesor().']';
  }

  public function getFlag() {
    return $this->getPaises()->getFlag();
  }
}
