<?php


abstract class BaseTraducciones extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_traduccion;


	
	protected $texto;

	
	protected $collIdiomatraduccions;

	
	protected $lastIdiomatraduccionCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdTraduccion()
	{

		return $this->id_traduccion;
	}

	
	public function getTexto()
	{

		return $this->texto;
	}

	
	public function setIdTraduccion($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id_traduccion !== $v) {
			$this->id_traduccion = $v;
			$this->modifiedColumns[] = TraduccionesPeer::ID_TRADUCCION;
		}

	} 
	
	public function setTexto($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->texto !== $v) {
			$this->texto = $v;
			$this->modifiedColumns[] = TraduccionesPeer::TEXTO;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_traduccion = $rs->getString($startcol + 0);

			$this->texto = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Traducciones object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TraduccionesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TraduccionesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TraduccionesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TraduccionesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdTraduccion($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += TraduccionesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collIdiomatraduccions !== null) {
				foreach($this->collIdiomatraduccions as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = TraduccionesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collIdiomatraduccions !== null) {
					foreach($this->collIdiomatraduccions as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TraduccionesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdTraduccion();
				break;
			case 1:
				return $this->getTexto();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TraduccionesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdTraduccion(),
			$keys[1] => $this->getTexto(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TraduccionesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdTraduccion($value);
				break;
			case 1:
				$this->setTexto($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TraduccionesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdTraduccion($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTexto($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TraduccionesPeer::DATABASE_NAME);

		if ($this->isColumnModified(TraduccionesPeer::ID_TRADUCCION)) $criteria->add(TraduccionesPeer::ID_TRADUCCION, $this->id_traduccion);
		if ($this->isColumnModified(TraduccionesPeer::TEXTO)) $criteria->add(TraduccionesPeer::TEXTO, $this->texto);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TraduccionesPeer::DATABASE_NAME);

		$criteria->add(TraduccionesPeer::ID_TRADUCCION, $this->id_traduccion);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdTraduccion();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdTraduccion($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTexto($this->texto);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getIdiomatraduccions() as $relObj) {
				$copyObj->addIdiomatraduccion($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setIdTraduccion(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TraduccionesPeer();
		}
		return self::$peer;
	}

	
	public function initIdiomatraduccions()
	{
		if ($this->collIdiomatraduccions === null) {
			$this->collIdiomatraduccions = array();
		}
	}

	
	public function getIdiomatraduccions($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseIdiomatraduccionPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collIdiomatraduccions === null) {
			if ($this->isNew()) {
			   $this->collIdiomatraduccions = array();
			} else {

				$criteria->add(IdiomatraduccionPeer::ID_TRADUCCION, $this->getIdTraduccion());

				IdiomatraduccionPeer::addSelectColumns($criteria);
				$this->collIdiomatraduccions = IdiomatraduccionPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(IdiomatraduccionPeer::ID_TRADUCCION, $this->getIdTraduccion());

				IdiomatraduccionPeer::addSelectColumns($criteria);
				if (!isset($this->lastIdiomatraduccionCriteria) || !$this->lastIdiomatraduccionCriteria->equals($criteria)) {
					$this->collIdiomatraduccions = IdiomatraduccionPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastIdiomatraduccionCriteria = $criteria;
		return $this->collIdiomatraduccions;
	}

	
	public function countIdiomatraduccions($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseIdiomatraduccionPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(IdiomatraduccionPeer::ID_TRADUCCION, $this->getIdTraduccion());

		return IdiomatraduccionPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addIdiomatraduccion(Idiomatraduccion $l)
	{
		$this->collIdiomatraduccions[] = $l;
		$l->setTraducciones($this);
	}


	
	public function getIdiomatraduccionsJoinIdiomas($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseIdiomatraduccionPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collIdiomatraduccions === null) {
			if ($this->isNew()) {
				$this->collIdiomatraduccions = array();
			} else {

				$criteria->add(IdiomatraduccionPeer::ID_TRADUCCION, $this->getIdTraduccion());

				$this->collIdiomatraduccions = IdiomatraduccionPeer::doSelectJoinIdiomas($criteria, $con);
			}
		} else {
									
			$criteria->add(IdiomatraduccionPeer::ID_TRADUCCION, $this->getIdTraduccion());

			if (!isset($this->lastIdiomatraduccionCriteria) || !$this->lastIdiomatraduccionCriteria->equals($criteria)) {
				$this->collIdiomatraduccions = IdiomatraduccionPeer::doSelectJoinIdiomas($criteria, $con);
			}
		}
		$this->lastIdiomatraduccionCriteria = $criteria;

		return $this->collIdiomatraduccions;
	}

} 