<?php


abstract class BaseCalculos extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_tipo;


	
	protected $id_muestra;


	
	protected $id_parametro;


	
	protected $id_alumno;


	
	protected $resultado;


	
	protected $id_equipo;


	
	protected $id;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdTipo()
	{

		return $this->id_tipo;
	}

	
	public function getIdMuestra()
	{

		return $this->id_muestra;
	}

	
	public function getIdParametro()
	{

		return $this->id_parametro;
	}

	
	public function getIdAlumno()
	{

		return $this->id_alumno;
	}

	
	public function getResultado()
	{

		return $this->resultado;
	}

	
	public function getIdEquipo()
	{

		return $this->id_equipo;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function setIdTipo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_tipo !== $v) {
			$this->id_tipo = $v;
			$this->modifiedColumns[] = CalculosPeer::ID_TIPO;
		}

	} 
	
	public function setIdMuestra($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_muestra !== $v) {
			$this->id_muestra = $v;
			$this->modifiedColumns[] = CalculosPeer::ID_MUESTRA;
		}

	} 
	
	public function setIdParametro($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_parametro !== $v) {
			$this->id_parametro = $v;
			$this->modifiedColumns[] = CalculosPeer::ID_PARAMETRO;
		}

	} 
	
	public function setIdAlumno($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_alumno !== $v) {
			$this->id_alumno = $v;
			$this->modifiedColumns[] = CalculosPeer::ID_ALUMNO;
		}

	} 
	
	public function setResultado($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->resultado !== $v) {
			$this->resultado = $v;
			$this->modifiedColumns[] = CalculosPeer::RESULTADO;
		}

	} 
	
	public function setIdEquipo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_equipo !== $v) {
			$this->id_equipo = $v;
			$this->modifiedColumns[] = CalculosPeer::ID_EQUIPO;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = CalculosPeer::ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_tipo = $rs->getInt($startcol + 0);

			$this->id_muestra = $rs->getInt($startcol + 1);

			$this->id_parametro = $rs->getInt($startcol + 2);

			$this->id_alumno = $rs->getInt($startcol + 3);

			$this->resultado = $rs->getString($startcol + 4);

			$this->id_equipo = $rs->getInt($startcol + 5);

			$this->id = $rs->getInt($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Calculos object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CalculosPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			CalculosPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CalculosPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = CalculosPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += CalculosPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = CalculosPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CalculosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdTipo();
				break;
			case 1:
				return $this->getIdMuestra();
				break;
			case 2:
				return $this->getIdParametro();
				break;
			case 3:
				return $this->getIdAlumno();
				break;
			case 4:
				return $this->getResultado();
				break;
			case 5:
				return $this->getIdEquipo();
				break;
			case 6:
				return $this->getId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CalculosPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdTipo(),
			$keys[1] => $this->getIdMuestra(),
			$keys[2] => $this->getIdParametro(),
			$keys[3] => $this->getIdAlumno(),
			$keys[4] => $this->getResultado(),
			$keys[5] => $this->getIdEquipo(),
			$keys[6] => $this->getId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CalculosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdTipo($value);
				break;
			case 1:
				$this->setIdMuestra($value);
				break;
			case 2:
				$this->setIdParametro($value);
				break;
			case 3:
				$this->setIdAlumno($value);
				break;
			case 4:
				$this->setResultado($value);
				break;
			case 5:
				$this->setIdEquipo($value);
				break;
			case 6:
				$this->setId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CalculosPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdTipo($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdMuestra($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIdParametro($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIdAlumno($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setResultado($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setIdEquipo($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setId($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(CalculosPeer::DATABASE_NAME);

		if ($this->isColumnModified(CalculosPeer::ID_TIPO)) $criteria->add(CalculosPeer::ID_TIPO, $this->id_tipo);
		if ($this->isColumnModified(CalculosPeer::ID_MUESTRA)) $criteria->add(CalculosPeer::ID_MUESTRA, $this->id_muestra);
		if ($this->isColumnModified(CalculosPeer::ID_PARAMETRO)) $criteria->add(CalculosPeer::ID_PARAMETRO, $this->id_parametro);
		if ($this->isColumnModified(CalculosPeer::ID_ALUMNO)) $criteria->add(CalculosPeer::ID_ALUMNO, $this->id_alumno);
		if ($this->isColumnModified(CalculosPeer::RESULTADO)) $criteria->add(CalculosPeer::RESULTADO, $this->resultado);
		if ($this->isColumnModified(CalculosPeer::ID_EQUIPO)) $criteria->add(CalculosPeer::ID_EQUIPO, $this->id_equipo);
		if ($this->isColumnModified(CalculosPeer::ID)) $criteria->add(CalculosPeer::ID, $this->id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(CalculosPeer::DATABASE_NAME);

		$criteria->add(CalculosPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdTipo($this->id_tipo);

		$copyObj->setIdMuestra($this->id_muestra);

		$copyObj->setIdParametro($this->id_parametro);

		$copyObj->setIdAlumno($this->id_alumno);

		$copyObj->setResultado($this->resultado);

		$copyObj->setIdEquipo($this->id_equipo);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new CalculosPeer();
		}
		return self::$peer;
	}

} 