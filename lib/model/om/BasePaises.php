<?php


abstract class BasePaises extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_pais;


	
	protected $pais;


	
	protected $codigo;

	
	protected $collEquiposs;

	
	protected $lastEquiposCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdPais()
	{

		return $this->id_pais;
	}

	
	public function getPais()
	{

		return $this->pais;
	}

	
	public function getCodigo()
	{

		return $this->codigo;
	}

	
	public function setIdPais($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_pais !== $v) {
			$this->id_pais = $v;
			$this->modifiedColumns[] = PaisesPeer::ID_PAIS;
		}

	} 
	
	public function setPais($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pais !== $v) {
			$this->pais = $v;
			$this->modifiedColumns[] = PaisesPeer::PAIS;
		}

	} 
	
	public function setCodigo($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->codigo !== $v) {
			$this->codigo = $v;
			$this->modifiedColumns[] = PaisesPeer::CODIGO;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_pais = $rs->getInt($startcol + 0);

			$this->pais = $rs->getString($startcol + 1);

			$this->codigo = $rs->getString($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Paises object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PaisesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PaisesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PaisesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PaisesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdPais($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PaisesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collEquiposs !== null) {
				foreach($this->collEquiposs as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PaisesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collEquiposs !== null) {
					foreach($this->collEquiposs as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PaisesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdPais();
				break;
			case 1:
				return $this->getPais();
				break;
			case 2:
				return $this->getCodigo();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PaisesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdPais(),
			$keys[1] => $this->getPais(),
			$keys[2] => $this->getCodigo(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PaisesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdPais($value);
				break;
			case 1:
				$this->setPais($value);
				break;
			case 2:
				$this->setCodigo($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PaisesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdPais($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPais($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCodigo($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PaisesPeer::DATABASE_NAME);

		if ($this->isColumnModified(PaisesPeer::ID_PAIS)) $criteria->add(PaisesPeer::ID_PAIS, $this->id_pais);
		if ($this->isColumnModified(PaisesPeer::PAIS)) $criteria->add(PaisesPeer::PAIS, $this->pais);
		if ($this->isColumnModified(PaisesPeer::CODIGO)) $criteria->add(PaisesPeer::CODIGO, $this->codigo);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PaisesPeer::DATABASE_NAME);

		$criteria->add(PaisesPeer::ID_PAIS, $this->id_pais);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdPais();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdPais($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setPais($this->pais);

		$copyObj->setCodigo($this->codigo);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getEquiposs() as $relObj) {
				$copyObj->addEquipos($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setIdPais(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PaisesPeer();
		}
		return self::$peer;
	}

	
	public function initEquiposs()
	{
		if ($this->collEquiposs === null) {
			$this->collEquiposs = array();
		}
	}

	
	public function getEquiposs($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseEquiposPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collEquiposs === null) {
			if ($this->isNew()) {
			   $this->collEquiposs = array();
			} else {

				$criteria->add(EquiposPeer::ID_PAIS, $this->getIdPais());

				EquiposPeer::addSelectColumns($criteria);
				$this->collEquiposs = EquiposPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(EquiposPeer::ID_PAIS, $this->getIdPais());

				EquiposPeer::addSelectColumns($criteria);
				if (!isset($this->lastEquiposCriteria) || !$this->lastEquiposCriteria->equals($criteria)) {
					$this->collEquiposs = EquiposPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastEquiposCriteria = $criteria;
		return $this->collEquiposs;
	}

	
	public function countEquiposs($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseEquiposPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(EquiposPeer::ID_PAIS, $this->getIdPais());

		return EquiposPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addEquipos(Equipos $l)
	{
		$this->collEquiposs[] = $l;
		$l->setPaises($this);
	}

} 