<?php


abstract class BaseJueces extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_juez;


	
	protected $nombre;

	
	protected $collResultadojuezs;

	
	protected $lastResultadojuezCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdJuez()
	{

		return $this->id_juez;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function setIdJuez($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_juez !== $v) {
			$this->id_juez = $v;
			$this->modifiedColumns[] = JuecesPeer::ID_JUEZ;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = JuecesPeer::NOMBRE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_juez = $rs->getInt($startcol + 0);

			$this->nombre = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Jueces object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(JuecesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			JuecesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(JuecesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = JuecesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdJuez($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += JuecesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collResultadojuezs !== null) {
				foreach($this->collResultadojuezs as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = JuecesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collResultadojuezs !== null) {
					foreach($this->collResultadojuezs as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = JuecesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdJuez();
				break;
			case 1:
				return $this->getNombre();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = JuecesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdJuez(),
			$keys[1] => $this->getNombre(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = JuecesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdJuez($value);
				break;
			case 1:
				$this->setNombre($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = JuecesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdJuez($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(JuecesPeer::DATABASE_NAME);

		if ($this->isColumnModified(JuecesPeer::ID_JUEZ)) $criteria->add(JuecesPeer::ID_JUEZ, $this->id_juez);
		if ($this->isColumnModified(JuecesPeer::NOMBRE)) $criteria->add(JuecesPeer::NOMBRE, $this->nombre);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(JuecesPeer::DATABASE_NAME);

		$criteria->add(JuecesPeer::ID_JUEZ, $this->id_juez);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdJuez();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdJuez($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNombre($this->nombre);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getResultadojuezs() as $relObj) {
				$copyObj->addResultadojuez($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setIdJuez(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new JuecesPeer();
		}
		return self::$peer;
	}

	
	public function initResultadojuezs()
	{
		if ($this->collResultadojuezs === null) {
			$this->collResultadojuezs = array();
		}
	}

	
	public function getResultadojuezs($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadojuezs === null) {
			if ($this->isNew()) {
			   $this->collResultadojuezs = array();
			} else {

				$criteria->add(ResultadojuezPeer::ID_JUEZ, $this->getIdJuez());

				ResultadojuezPeer::addSelectColumns($criteria);
				$this->collResultadojuezs = ResultadojuezPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ResultadojuezPeer::ID_JUEZ, $this->getIdJuez());

				ResultadojuezPeer::addSelectColumns($criteria);
				if (!isset($this->lastResultadojuezCriteria) || !$this->lastResultadojuezCriteria->equals($criteria)) {
					$this->collResultadojuezs = ResultadojuezPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastResultadojuezCriteria = $criteria;
		return $this->collResultadojuezs;
	}

	
	public function countResultadojuezs($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ResultadojuezPeer::ID_JUEZ, $this->getIdJuez());

		return ResultadojuezPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addResultadojuez(Resultadojuez $l)
	{
		$this->collResultadojuezs[] = $l;
		$l->setJueces($this);
	}


	
	public function getResultadojuezsJoinMuestras($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadojuezs === null) {
			if ($this->isNew()) {
				$this->collResultadojuezs = array();
			} else {

				$criteria->add(ResultadojuezPeer::ID_JUEZ, $this->getIdJuez());

				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinMuestras($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadojuezPeer::ID_JUEZ, $this->getIdJuez());

			if (!isset($this->lastResultadojuezCriteria) || !$this->lastResultadojuezCriteria->equals($criteria)) {
				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinMuestras($criteria, $con);
			}
		}
		$this->lastResultadojuezCriteria = $criteria;

		return $this->collResultadojuezs;
	}


	
	public function getResultadojuezsJoinParametros($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadojuezs === null) {
			if ($this->isNew()) {
				$this->collResultadojuezs = array();
			} else {

				$criteria->add(ResultadojuezPeer::ID_JUEZ, $this->getIdJuez());

				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinParametros($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadojuezPeer::ID_JUEZ, $this->getIdJuez());

			if (!isset($this->lastResultadojuezCriteria) || !$this->lastResultadojuezCriteria->equals($criteria)) {
				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinParametros($criteria, $con);
			}
		}
		$this->lastResultadojuezCriteria = $criteria;

		return $this->collResultadojuezs;
	}

} 