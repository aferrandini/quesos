<?php


abstract class BaseIdiomas extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_idioma;


	
	protected $culture = 'null';


	
	protected $nombre;

	
	protected $collIdiomatraduccions;

	
	protected $lastIdiomatraduccionCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdIdioma()
	{

		return $this->id_idioma;
	}

	
	public function getCulture()
	{

		return $this->culture;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function setIdIdioma($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_idioma !== $v) {
			$this->id_idioma = $v;
			$this->modifiedColumns[] = IdiomasPeer::ID_IDIOMA;
		}

	} 
	
	public function setCulture($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->culture !== $v || $v === 'null') {
			$this->culture = $v;
			$this->modifiedColumns[] = IdiomasPeer::CULTURE;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = IdiomasPeer::NOMBRE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_idioma = $rs->getInt($startcol + 0);

			$this->culture = $rs->getString($startcol + 1);

			$this->nombre = $rs->getString($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Idiomas object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(IdiomasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			IdiomasPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(IdiomasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = IdiomasPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdIdioma($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += IdiomasPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collIdiomatraduccions !== null) {
				foreach($this->collIdiomatraduccions as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = IdiomasPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collIdiomatraduccions !== null) {
					foreach($this->collIdiomatraduccions as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = IdiomasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdIdioma();
				break;
			case 1:
				return $this->getCulture();
				break;
			case 2:
				return $this->getNombre();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = IdiomasPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdIdioma(),
			$keys[1] => $this->getCulture(),
			$keys[2] => $this->getNombre(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = IdiomasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdIdioma($value);
				break;
			case 1:
				$this->setCulture($value);
				break;
			case 2:
				$this->setNombre($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = IdiomasPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdIdioma($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCulture($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNombre($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(IdiomasPeer::DATABASE_NAME);

		if ($this->isColumnModified(IdiomasPeer::ID_IDIOMA)) $criteria->add(IdiomasPeer::ID_IDIOMA, $this->id_idioma);
		if ($this->isColumnModified(IdiomasPeer::CULTURE)) $criteria->add(IdiomasPeer::CULTURE, $this->culture);
		if ($this->isColumnModified(IdiomasPeer::NOMBRE)) $criteria->add(IdiomasPeer::NOMBRE, $this->nombre);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(IdiomasPeer::DATABASE_NAME);

		$criteria->add(IdiomasPeer::ID_IDIOMA, $this->id_idioma);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdIdioma();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdIdioma($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCulture($this->culture);

		$copyObj->setNombre($this->nombre);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getIdiomatraduccions() as $relObj) {
				$copyObj->addIdiomatraduccion($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setIdIdioma(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new IdiomasPeer();
		}
		return self::$peer;
	}

	
	public function initIdiomatraduccions()
	{
		if ($this->collIdiomatraduccions === null) {
			$this->collIdiomatraduccions = array();
		}
	}

	
	public function getIdiomatraduccions($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseIdiomatraduccionPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collIdiomatraduccions === null) {
			if ($this->isNew()) {
			   $this->collIdiomatraduccions = array();
			} else {

				$criteria->add(IdiomatraduccionPeer::ID_IDIOMA, $this->getIdIdioma());

				IdiomatraduccionPeer::addSelectColumns($criteria);
				$this->collIdiomatraduccions = IdiomatraduccionPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(IdiomatraduccionPeer::ID_IDIOMA, $this->getIdIdioma());

				IdiomatraduccionPeer::addSelectColumns($criteria);
				if (!isset($this->lastIdiomatraduccionCriteria) || !$this->lastIdiomatraduccionCriteria->equals($criteria)) {
					$this->collIdiomatraduccions = IdiomatraduccionPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastIdiomatraduccionCriteria = $criteria;
		return $this->collIdiomatraduccions;
	}

	
	public function countIdiomatraduccions($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseIdiomatraduccionPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(IdiomatraduccionPeer::ID_IDIOMA, $this->getIdIdioma());

		return IdiomatraduccionPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addIdiomatraduccion(Idiomatraduccion $l)
	{
		$this->collIdiomatraduccions[] = $l;
		$l->setIdiomas($this);
	}


	
	public function getIdiomatraduccionsJoinTraducciones($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseIdiomatraduccionPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collIdiomatraduccions === null) {
			if ($this->isNew()) {
				$this->collIdiomatraduccions = array();
			} else {

				$criteria->add(IdiomatraduccionPeer::ID_IDIOMA, $this->getIdIdioma());

				$this->collIdiomatraduccions = IdiomatraduccionPeer::doSelectJoinTraducciones($criteria, $con);
			}
		} else {
									
			$criteria->add(IdiomatraduccionPeer::ID_IDIOMA, $this->getIdIdioma());

			if (!isset($this->lastIdiomatraduccionCriteria) || !$this->lastIdiomatraduccionCriteria->equals($criteria)) {
				$this->collIdiomatraduccions = IdiomatraduccionPeer::doSelectJoinTraducciones($criteria, $con);
			}
		}
		$this->lastIdiomatraduccionCriteria = $criteria;

		return $this->collIdiomatraduccions;
	}

} 