<?php


abstract class BaseMuestras extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_muestra;


	
	protected $nombre;


	
	protected $id_tipo;

	
	protected $aTipomuestra;

	
	protected $collResultadojuezs;

	
	protected $lastResultadojuezCriteria = null;

	
	protected $collResultadoss;

	
	protected $lastResultadosCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdMuestra()
	{

		return $this->id_muestra;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function getIdTipo()
	{

		return $this->id_tipo;
	}

	
	public function setIdMuestra($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_muestra !== $v) {
			$this->id_muestra = $v;
			$this->modifiedColumns[] = MuestrasPeer::ID_MUESTRA;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = MuestrasPeer::NOMBRE;
		}

	} 
	
	public function setIdTipo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_tipo !== $v) {
			$this->id_tipo = $v;
			$this->modifiedColumns[] = MuestrasPeer::ID_TIPO;
		}

		if ($this->aTipomuestra !== null && $this->aTipomuestra->getIdTipomuestra() !== $v) {
			$this->aTipomuestra = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_muestra = $rs->getInt($startcol + 0);

			$this->nombre = $rs->getString($startcol + 1);

			$this->id_tipo = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Muestras object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MuestrasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MuestrasPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MuestrasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTipomuestra !== null) {
				if ($this->aTipomuestra->isModified()) {
					$affectedRows += $this->aTipomuestra->save($con);
				}
				$this->setTipomuestra($this->aTipomuestra);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MuestrasPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdMuestra($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += MuestrasPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collResultadojuezs !== null) {
				foreach($this->collResultadojuezs as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collResultadoss !== null) {
				foreach($this->collResultadoss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTipomuestra !== null) {
				if (!$this->aTipomuestra->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTipomuestra->getValidationFailures());
				}
			}


			if (($retval = MuestrasPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collResultadojuezs !== null) {
					foreach($this->collResultadojuezs as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collResultadoss !== null) {
					foreach($this->collResultadoss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MuestrasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdMuestra();
				break;
			case 1:
				return $this->getNombre();
				break;
			case 2:
				return $this->getIdTipo();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MuestrasPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdMuestra(),
			$keys[1] => $this->getNombre(),
			$keys[2] => $this->getIdTipo(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MuestrasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdMuestra($value);
				break;
			case 1:
				$this->setNombre($value);
				break;
			case 2:
				$this->setIdTipo($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MuestrasPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdMuestra($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIdTipo($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MuestrasPeer::DATABASE_NAME);

		if ($this->isColumnModified(MuestrasPeer::ID_MUESTRA)) $criteria->add(MuestrasPeer::ID_MUESTRA, $this->id_muestra);
		if ($this->isColumnModified(MuestrasPeer::NOMBRE)) $criteria->add(MuestrasPeer::NOMBRE, $this->nombre);
		if ($this->isColumnModified(MuestrasPeer::ID_TIPO)) $criteria->add(MuestrasPeer::ID_TIPO, $this->id_tipo);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MuestrasPeer::DATABASE_NAME);

		$criteria->add(MuestrasPeer::ID_MUESTRA, $this->id_muestra);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdMuestra();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdMuestra($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNombre($this->nombre);

		$copyObj->setIdTipo($this->id_tipo);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getResultadojuezs() as $relObj) {
				$copyObj->addResultadojuez($relObj->copy($deepCopy));
			}

			foreach($this->getResultadoss() as $relObj) {
				$copyObj->addResultados($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setIdMuestra(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MuestrasPeer();
		}
		return self::$peer;
	}

	
	public function setTipomuestra($v)
	{


		if ($v === null) {
			$this->setIdTipo(NULL);
		} else {
			$this->setIdTipo($v->getIdTipomuestra());
		}


		$this->aTipomuestra = $v;
	}


	
	public function getTipomuestra($con = null)
	{
				include_once 'lib/model/om/BaseTipomuestraPeer.php';

		if ($this->aTipomuestra === null && ($this->id_tipo !== null)) {

			$this->aTipomuestra = TipomuestraPeer::retrieveByPK($this->id_tipo, $con);

			
		}
		return $this->aTipomuestra;
	}

	
	public function initResultadojuezs()
	{
		if ($this->collResultadojuezs === null) {
			$this->collResultadojuezs = array();
		}
	}

	
	public function getResultadojuezs($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadojuezs === null) {
			if ($this->isNew()) {
			   $this->collResultadojuezs = array();
			} else {

				$criteria->add(ResultadojuezPeer::ID_MUESTRA, $this->getIdMuestra());

				ResultadojuezPeer::addSelectColumns($criteria);
				$this->collResultadojuezs = ResultadojuezPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ResultadojuezPeer::ID_MUESTRA, $this->getIdMuestra());

				ResultadojuezPeer::addSelectColumns($criteria);
				if (!isset($this->lastResultadojuezCriteria) || !$this->lastResultadojuezCriteria->equals($criteria)) {
					$this->collResultadojuezs = ResultadojuezPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastResultadojuezCriteria = $criteria;
		return $this->collResultadojuezs;
	}

	
	public function countResultadojuezs($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ResultadojuezPeer::ID_MUESTRA, $this->getIdMuestra());

		return ResultadojuezPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addResultadojuez(Resultadojuez $l)
	{
		$this->collResultadojuezs[] = $l;
		$l->setMuestras($this);
	}


	
	public function getResultadojuezsJoinJueces($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadojuezs === null) {
			if ($this->isNew()) {
				$this->collResultadojuezs = array();
			} else {

				$criteria->add(ResultadojuezPeer::ID_MUESTRA, $this->getIdMuestra());

				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinJueces($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadojuezPeer::ID_MUESTRA, $this->getIdMuestra());

			if (!isset($this->lastResultadojuezCriteria) || !$this->lastResultadojuezCriteria->equals($criteria)) {
				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinJueces($criteria, $con);
			}
		}
		$this->lastResultadojuezCriteria = $criteria;

		return $this->collResultadojuezs;
	}


	
	public function getResultadojuezsJoinParametros($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadojuezs === null) {
			if ($this->isNew()) {
				$this->collResultadojuezs = array();
			} else {

				$criteria->add(ResultadojuezPeer::ID_MUESTRA, $this->getIdMuestra());

				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinParametros($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadojuezPeer::ID_MUESTRA, $this->getIdMuestra());

			if (!isset($this->lastResultadojuezCriteria) || !$this->lastResultadojuezCriteria->equals($criteria)) {
				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinParametros($criteria, $con);
			}
		}
		$this->lastResultadojuezCriteria = $criteria;

		return $this->collResultadojuezs;
	}

	
	public function initResultadoss()
	{
		if ($this->collResultadoss === null) {
			$this->collResultadoss = array();
		}
	}

	
	public function getResultadoss($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadoss === null) {
			if ($this->isNew()) {
			   $this->collResultadoss = array();
			} else {

				$criteria->add(ResultadosPeer::ID_MUESTRA, $this->getIdMuestra());

				ResultadosPeer::addSelectColumns($criteria);
				$this->collResultadoss = ResultadosPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ResultadosPeer::ID_MUESTRA, $this->getIdMuestra());

				ResultadosPeer::addSelectColumns($criteria);
				if (!isset($this->lastResultadosCriteria) || !$this->lastResultadosCriteria->equals($criteria)) {
					$this->collResultadoss = ResultadosPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastResultadosCriteria = $criteria;
		return $this->collResultadoss;
	}

	
	public function countResultadoss($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ResultadosPeer::ID_MUESTRA, $this->getIdMuestra());

		return ResultadosPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addResultados(Resultados $l)
	{
		$this->collResultadoss[] = $l;
		$l->setMuestras($this);
	}


	
	public function getResultadossJoinAlumnos($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadoss === null) {
			if ($this->isNew()) {
				$this->collResultadoss = array();
			} else {

				$criteria->add(ResultadosPeer::ID_MUESTRA, $this->getIdMuestra());

				$this->collResultadoss = ResultadosPeer::doSelectJoinAlumnos($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadosPeer::ID_MUESTRA, $this->getIdMuestra());

			if (!isset($this->lastResultadosCriteria) || !$this->lastResultadosCriteria->equals($criteria)) {
				$this->collResultadoss = ResultadosPeer::doSelectJoinAlumnos($criteria, $con);
			}
		}
		$this->lastResultadosCriteria = $criteria;

		return $this->collResultadoss;
	}


	
	public function getResultadossJoinParametros($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadoss === null) {
			if ($this->isNew()) {
				$this->collResultadoss = array();
			} else {

				$criteria->add(ResultadosPeer::ID_MUESTRA, $this->getIdMuestra());

				$this->collResultadoss = ResultadosPeer::doSelectJoinParametros($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadosPeer::ID_MUESTRA, $this->getIdMuestra());

			if (!isset($this->lastResultadosCriteria) || !$this->lastResultadosCriteria->equals($criteria)) {
				$this->collResultadoss = ResultadosPeer::doSelectJoinParametros($criteria, $con);
			}
		}
		$this->lastResultadosCriteria = $criteria;

		return $this->collResultadoss;
	}

} 