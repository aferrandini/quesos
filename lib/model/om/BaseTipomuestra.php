<?php


abstract class BaseTipomuestra extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_tipomuestra;


	
	protected $nombre;

	
	protected $collMuestrass;

	
	protected $lastMuestrasCriteria = null;

	
	protected $collParametross;

	
	protected $lastParametrosCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdTipomuestra()
	{

		return $this->id_tipomuestra;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function setIdTipomuestra($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_tipomuestra !== $v) {
			$this->id_tipomuestra = $v;
			$this->modifiedColumns[] = TipomuestraPeer::ID_TIPOMUESTRA;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = TipomuestraPeer::NOMBRE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_tipomuestra = $rs->getInt($startcol + 0);

			$this->nombre = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Tipomuestra object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TipomuestraPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TipomuestraPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TipomuestraPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TipomuestraPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdTipomuestra($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += TipomuestraPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collMuestrass !== null) {
				foreach($this->collMuestrass as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collParametross !== null) {
				foreach($this->collParametross as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = TipomuestraPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collMuestrass !== null) {
					foreach($this->collMuestrass as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collParametross !== null) {
					foreach($this->collParametross as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TipomuestraPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdTipomuestra();
				break;
			case 1:
				return $this->getNombre();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TipomuestraPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdTipomuestra(),
			$keys[1] => $this->getNombre(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TipomuestraPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdTipomuestra($value);
				break;
			case 1:
				$this->setNombre($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TipomuestraPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdTipomuestra($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TipomuestraPeer::DATABASE_NAME);

		if ($this->isColumnModified(TipomuestraPeer::ID_TIPOMUESTRA)) $criteria->add(TipomuestraPeer::ID_TIPOMUESTRA, $this->id_tipomuestra);
		if ($this->isColumnModified(TipomuestraPeer::NOMBRE)) $criteria->add(TipomuestraPeer::NOMBRE, $this->nombre);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TipomuestraPeer::DATABASE_NAME);

		$criteria->add(TipomuestraPeer::ID_TIPOMUESTRA, $this->id_tipomuestra);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdTipomuestra();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdTipomuestra($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNombre($this->nombre);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getMuestrass() as $relObj) {
				$copyObj->addMuestras($relObj->copy($deepCopy));
			}

			foreach($this->getParametross() as $relObj) {
				$copyObj->addParametros($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setIdTipomuestra(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TipomuestraPeer();
		}
		return self::$peer;
	}

	
	public function initMuestrass()
	{
		if ($this->collMuestrass === null) {
			$this->collMuestrass = array();
		}
	}

	
	public function getMuestrass($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseMuestrasPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collMuestrass === null) {
			if ($this->isNew()) {
			   $this->collMuestrass = array();
			} else {

				$criteria->add(MuestrasPeer::ID_TIPO, $this->getIdTipomuestra());

				MuestrasPeer::addSelectColumns($criteria);
				$this->collMuestrass = MuestrasPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(MuestrasPeer::ID_TIPO, $this->getIdTipomuestra());

				MuestrasPeer::addSelectColumns($criteria);
				if (!isset($this->lastMuestrasCriteria) || !$this->lastMuestrasCriteria->equals($criteria)) {
					$this->collMuestrass = MuestrasPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastMuestrasCriteria = $criteria;
		return $this->collMuestrass;
	}

	
	public function countMuestrass($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseMuestrasPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(MuestrasPeer::ID_TIPO, $this->getIdTipomuestra());

		return MuestrasPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addMuestras(Muestras $l)
	{
		$this->collMuestrass[] = $l;
		$l->setTipomuestra($this);
	}

	
	public function initParametross()
	{
		if ($this->collParametross === null) {
			$this->collParametross = array();
		}
	}

	
	public function getParametross($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseParametrosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collParametross === null) {
			if ($this->isNew()) {
			   $this->collParametross = array();
			} else {

				$criteria->add(ParametrosPeer::ID_TIPOMUESTRA, $this->getIdTipomuestra());

				ParametrosPeer::addSelectColumns($criteria);
				$this->collParametross = ParametrosPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ParametrosPeer::ID_TIPOMUESTRA, $this->getIdTipomuestra());

				ParametrosPeer::addSelectColumns($criteria);
				if (!isset($this->lastParametrosCriteria) || !$this->lastParametrosCriteria->equals($criteria)) {
					$this->collParametross = ParametrosPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastParametrosCriteria = $criteria;
		return $this->collParametross;
	}

	
	public function countParametross($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseParametrosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ParametrosPeer::ID_TIPOMUESTRA, $this->getIdTipomuestra());

		return ParametrosPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addParametros(Parametros $l)
	{
		$this->collParametross[] = $l;
		$l->setTipomuestra($this);
	}


	
	public function getParametrossJoinFases($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseParametrosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collParametross === null) {
			if ($this->isNew()) {
				$this->collParametross = array();
			} else {

				$criteria->add(ParametrosPeer::ID_TIPOMUESTRA, $this->getIdTipomuestra());

				$this->collParametross = ParametrosPeer::doSelectJoinFases($criteria, $con);
			}
		} else {
									
			$criteria->add(ParametrosPeer::ID_TIPOMUESTRA, $this->getIdTipomuestra());

			if (!isset($this->lastParametrosCriteria) || !$this->lastParametrosCriteria->equals($criteria)) {
				$this->collParametross = ParametrosPeer::doSelectJoinFases($criteria, $con);
			}
		}
		$this->lastParametrosCriteria = $criteria;

		return $this->collParametross;
	}


	
	public function getParametrossJoinSeparadores($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseParametrosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collParametross === null) {
			if ($this->isNew()) {
				$this->collParametross = array();
			} else {

				$criteria->add(ParametrosPeer::ID_TIPOMUESTRA, $this->getIdTipomuestra());

				$this->collParametross = ParametrosPeer::doSelectJoinSeparadores($criteria, $con);
			}
		} else {
									
			$criteria->add(ParametrosPeer::ID_TIPOMUESTRA, $this->getIdTipomuestra());

			if (!isset($this->lastParametrosCriteria) || !$this->lastParametrosCriteria->equals($criteria)) {
				$this->collParametross = ParametrosPeer::doSelectJoinSeparadores($criteria, $con);
			}
		}
		$this->lastParametrosCriteria = $criteria;

		return $this->collParametross;
	}

} 