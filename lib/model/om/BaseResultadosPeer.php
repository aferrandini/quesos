<?php


abstract class BaseResultadosPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'resultados';

	
	const CLASS_DEFAULT = 'lib.model.Resultados';

	
	const NUM_COLUMNS = 6;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_RESULTADO = 'resultados.ID_RESULTADO';

	
	const ID_ALUMNO = 'resultados.ID_ALUMNO';

	
	const ID_MUESTRA = 'resultados.ID_MUESTRA';

	
	const ID_PARAMETRO = 'resultados.ID_PARAMETRO';

	
	const RESPUESTA = 'resultados.RESPUESTA';

	
	const PUNISH = 'resultados.PUNISH';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdResultado', 'IdAlumno', 'IdMuestra', 'IdParametro', 'Respuesta', 'Punish', ),
		BasePeer::TYPE_COLNAME => array (ResultadosPeer::ID_RESULTADO, ResultadosPeer::ID_ALUMNO, ResultadosPeer::ID_MUESTRA, ResultadosPeer::ID_PARAMETRO, ResultadosPeer::RESPUESTA, ResultadosPeer::PUNISH, ),
		BasePeer::TYPE_FIELDNAME => array ('id_resultado', 'id_alumno', 'id_muestra', 'id_parametro', 'respuesta', 'punish', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdResultado' => 0, 'IdAlumno' => 1, 'IdMuestra' => 2, 'IdParametro' => 3, 'Respuesta' => 4, 'Punish' => 5, ),
		BasePeer::TYPE_COLNAME => array (ResultadosPeer::ID_RESULTADO => 0, ResultadosPeer::ID_ALUMNO => 1, ResultadosPeer::ID_MUESTRA => 2, ResultadosPeer::ID_PARAMETRO => 3, ResultadosPeer::RESPUESTA => 4, ResultadosPeer::PUNISH => 5, ),
		BasePeer::TYPE_FIELDNAME => array ('id_resultado' => 0, 'id_alumno' => 1, 'id_muestra' => 2, 'id_parametro' => 3, 'respuesta' => 4, 'punish' => 5, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ResultadosMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ResultadosMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ResultadosPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ResultadosPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ResultadosPeer::ID_RESULTADO);

		$criteria->addSelectColumn(ResultadosPeer::ID_ALUMNO);

		$criteria->addSelectColumn(ResultadosPeer::ID_MUESTRA);

		$criteria->addSelectColumn(ResultadosPeer::ID_PARAMETRO);

		$criteria->addSelectColumn(ResultadosPeer::RESPUESTA);

		$criteria->addSelectColumn(ResultadosPeer::PUNISH);

	}

	const COUNT = 'COUNT(resultados.ID_RESULTADO)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT resultados.ID_RESULTADO)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ResultadosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ResultadosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ResultadosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ResultadosPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ResultadosPeer::populateObjects(ResultadosPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ResultadosPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ResultadosPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinAlumnos(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ResultadosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ResultadosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ResultadosPeer::ID_ALUMNO, AlumnosPeer::ID_ALUMNO);

		$rs = ResultadosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinMuestras(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ResultadosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ResultadosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ResultadosPeer::ID_MUESTRA, MuestrasPeer::ID_MUESTRA);

		$rs = ResultadosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinParametros(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ResultadosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ResultadosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ResultadosPeer::ID_PARAMETRO, ParametrosPeer::ID_PARAMETRO);

		$rs = ResultadosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAlumnos(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ResultadosPeer::addSelectColumns($c);
		$startcol = (ResultadosPeer::NUM_COLUMNS - ResultadosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		AlumnosPeer::addSelectColumns($c);

		$c->addJoin(ResultadosPeer::ID_ALUMNO, AlumnosPeer::ID_ALUMNO);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ResultadosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = AlumnosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getAlumnos(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addResultados($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initResultadoss();
				$obj2->addResultados($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinMuestras(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ResultadosPeer::addSelectColumns($c);
		$startcol = (ResultadosPeer::NUM_COLUMNS - ResultadosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		MuestrasPeer::addSelectColumns($c);

		$c->addJoin(ResultadosPeer::ID_MUESTRA, MuestrasPeer::ID_MUESTRA);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ResultadosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MuestrasPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getMuestras(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addResultados($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initResultadoss();
				$obj2->addResultados($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinParametros(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ResultadosPeer::addSelectColumns($c);
		$startcol = (ResultadosPeer::NUM_COLUMNS - ResultadosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ParametrosPeer::addSelectColumns($c);

		$c->addJoin(ResultadosPeer::ID_PARAMETRO, ParametrosPeer::ID_PARAMETRO);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ResultadosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ParametrosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getParametros(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addResultados($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initResultadoss();
				$obj2->addResultados($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ResultadosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ResultadosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ResultadosPeer::ID_ALUMNO, AlumnosPeer::ID_ALUMNO);

		$criteria->addJoin(ResultadosPeer::ID_MUESTRA, MuestrasPeer::ID_MUESTRA);

		$criteria->addJoin(ResultadosPeer::ID_PARAMETRO, ParametrosPeer::ID_PARAMETRO);

		$rs = ResultadosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ResultadosPeer::addSelectColumns($c);
		$startcol2 = (ResultadosPeer::NUM_COLUMNS - ResultadosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		AlumnosPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + AlumnosPeer::NUM_COLUMNS;

		MuestrasPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + MuestrasPeer::NUM_COLUMNS;

		ParametrosPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + ParametrosPeer::NUM_COLUMNS;

		$c->addJoin(ResultadosPeer::ID_ALUMNO, AlumnosPeer::ID_ALUMNO);

		$c->addJoin(ResultadosPeer::ID_MUESTRA, MuestrasPeer::ID_MUESTRA);

		$c->addJoin(ResultadosPeer::ID_PARAMETRO, ParametrosPeer::ID_PARAMETRO);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ResultadosPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = AlumnosPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getAlumnos(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addResultados($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initResultadoss();
				$obj2->addResultados($obj1);
			}


					
			$omClass = MuestrasPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getMuestras(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addResultados($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initResultadoss();
				$obj3->addResultados($obj1);
			}


					
			$omClass = ParametrosPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getParametros(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addResultados($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj4->initResultadoss();
				$obj4->addResultados($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptAlumnos(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ResultadosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ResultadosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ResultadosPeer::ID_MUESTRA, MuestrasPeer::ID_MUESTRA);

		$criteria->addJoin(ResultadosPeer::ID_PARAMETRO, ParametrosPeer::ID_PARAMETRO);

		$rs = ResultadosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptMuestras(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ResultadosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ResultadosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ResultadosPeer::ID_ALUMNO, AlumnosPeer::ID_ALUMNO);

		$criteria->addJoin(ResultadosPeer::ID_PARAMETRO, ParametrosPeer::ID_PARAMETRO);

		$rs = ResultadosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptParametros(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ResultadosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ResultadosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ResultadosPeer::ID_ALUMNO, AlumnosPeer::ID_ALUMNO);

		$criteria->addJoin(ResultadosPeer::ID_MUESTRA, MuestrasPeer::ID_MUESTRA);

		$rs = ResultadosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptAlumnos(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ResultadosPeer::addSelectColumns($c);
		$startcol2 = (ResultadosPeer::NUM_COLUMNS - ResultadosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		MuestrasPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + MuestrasPeer::NUM_COLUMNS;

		ParametrosPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ParametrosPeer::NUM_COLUMNS;

		$c->addJoin(ResultadosPeer::ID_MUESTRA, MuestrasPeer::ID_MUESTRA);

		$c->addJoin(ResultadosPeer::ID_PARAMETRO, ParametrosPeer::ID_PARAMETRO);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ResultadosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MuestrasPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getMuestras(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addResultados($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initResultadoss();
				$obj2->addResultados($obj1);
			}

			$omClass = ParametrosPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getParametros(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addResultados($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initResultadoss();
				$obj3->addResultados($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptMuestras(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ResultadosPeer::addSelectColumns($c);
		$startcol2 = (ResultadosPeer::NUM_COLUMNS - ResultadosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		AlumnosPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + AlumnosPeer::NUM_COLUMNS;

		ParametrosPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ParametrosPeer::NUM_COLUMNS;

		$c->addJoin(ResultadosPeer::ID_ALUMNO, AlumnosPeer::ID_ALUMNO);

		$c->addJoin(ResultadosPeer::ID_PARAMETRO, ParametrosPeer::ID_PARAMETRO);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ResultadosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = AlumnosPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getAlumnos(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addResultados($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initResultadoss();
				$obj2->addResultados($obj1);
			}

			$omClass = ParametrosPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getParametros(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addResultados($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initResultadoss();
				$obj3->addResultados($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptParametros(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ResultadosPeer::addSelectColumns($c);
		$startcol2 = (ResultadosPeer::NUM_COLUMNS - ResultadosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		AlumnosPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + AlumnosPeer::NUM_COLUMNS;

		MuestrasPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + MuestrasPeer::NUM_COLUMNS;

		$c->addJoin(ResultadosPeer::ID_ALUMNO, AlumnosPeer::ID_ALUMNO);

		$c->addJoin(ResultadosPeer::ID_MUESTRA, MuestrasPeer::ID_MUESTRA);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ResultadosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = AlumnosPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getAlumnos(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addResultados($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initResultadoss();
				$obj2->addResultados($obj1);
			}

			$omClass = MuestrasPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getMuestras(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addResultados($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initResultadoss();
				$obj3->addResultados($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ResultadosPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ResultadosPeer::ID_RESULTADO); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ResultadosPeer::ID_RESULTADO);
			$selectCriteria->add(ResultadosPeer::ID_RESULTADO, $criteria->remove(ResultadosPeer::ID_RESULTADO), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ResultadosPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ResultadosPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Resultados) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ResultadosPeer::ID_RESULTADO, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Resultados $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ResultadosPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ResultadosPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ResultadosPeer::DATABASE_NAME, ResultadosPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ResultadosPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ResultadosPeer::DATABASE_NAME);

		$criteria->add(ResultadosPeer::ID_RESULTADO, $pk);


		$v = ResultadosPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ResultadosPeer::ID_RESULTADO, $pks, Criteria::IN);
			$objs = ResultadosPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseResultadosPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ResultadosMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ResultadosMapBuilder');
}
