<?php


abstract class BaseAlumnos extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_alumno;


	
	protected $nombre;


	
	protected $id_equipo;

	
	protected $aEquipos;

	
	protected $collResultadoss;

	
	protected $lastResultadosCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdAlumno()
	{

		return $this->id_alumno;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function getIdEquipo()
	{

		return $this->id_equipo;
	}

	
	public function setIdAlumno($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_alumno !== $v) {
			$this->id_alumno = $v;
			$this->modifiedColumns[] = AlumnosPeer::ID_ALUMNO;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = AlumnosPeer::NOMBRE;
		}

	} 
	
	public function setIdEquipo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_equipo !== $v) {
			$this->id_equipo = $v;
			$this->modifiedColumns[] = AlumnosPeer::ID_EQUIPO;
		}

		if ($this->aEquipos !== null && $this->aEquipos->getIdEquipo() !== $v) {
			$this->aEquipos = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_alumno = $rs->getInt($startcol + 0);

			$this->nombre = $rs->getString($startcol + 1);

			$this->id_equipo = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Alumnos object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AlumnosPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			AlumnosPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AlumnosPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aEquipos !== null) {
				if ($this->aEquipos->isModified()) {
					$affectedRows += $this->aEquipos->save($con);
				}
				$this->setEquipos($this->aEquipos);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = AlumnosPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdAlumno($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += AlumnosPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collResultadoss !== null) {
				foreach($this->collResultadoss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aEquipos !== null) {
				if (!$this->aEquipos->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aEquipos->getValidationFailures());
				}
			}


			if (($retval = AlumnosPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collResultadoss !== null) {
					foreach($this->collResultadoss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AlumnosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdAlumno();
				break;
			case 1:
				return $this->getNombre();
				break;
			case 2:
				return $this->getIdEquipo();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AlumnosPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdAlumno(),
			$keys[1] => $this->getNombre(),
			$keys[2] => $this->getIdEquipo(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AlumnosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdAlumno($value);
				break;
			case 1:
				$this->setNombre($value);
				break;
			case 2:
				$this->setIdEquipo($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AlumnosPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdAlumno($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIdEquipo($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(AlumnosPeer::DATABASE_NAME);

		if ($this->isColumnModified(AlumnosPeer::ID_ALUMNO)) $criteria->add(AlumnosPeer::ID_ALUMNO, $this->id_alumno);
		if ($this->isColumnModified(AlumnosPeer::NOMBRE)) $criteria->add(AlumnosPeer::NOMBRE, $this->nombre);
		if ($this->isColumnModified(AlumnosPeer::ID_EQUIPO)) $criteria->add(AlumnosPeer::ID_EQUIPO, $this->id_equipo);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(AlumnosPeer::DATABASE_NAME);

		$criteria->add(AlumnosPeer::ID_ALUMNO, $this->id_alumno);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdAlumno();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdAlumno($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNombre($this->nombre);

		$copyObj->setIdEquipo($this->id_equipo);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getResultadoss() as $relObj) {
				$copyObj->addResultados($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setIdAlumno(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new AlumnosPeer();
		}
		return self::$peer;
	}

	
	public function setEquipos($v)
	{


		if ($v === null) {
			$this->setIdEquipo(NULL);
		} else {
			$this->setIdEquipo($v->getIdEquipo());
		}


		$this->aEquipos = $v;
	}


	
	public function getEquipos($con = null)
	{
				include_once 'lib/model/om/BaseEquiposPeer.php';

		if ($this->aEquipos === null && ($this->id_equipo !== null)) {

			$this->aEquipos = EquiposPeer::retrieveByPK($this->id_equipo, $con);

			
		}
		return $this->aEquipos;
	}

	
	public function initResultadoss()
	{
		if ($this->collResultadoss === null) {
			$this->collResultadoss = array();
		}
	}

	
	public function getResultadoss($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadoss === null) {
			if ($this->isNew()) {
			   $this->collResultadoss = array();
			} else {

				$criteria->add(ResultadosPeer::ID_ALUMNO, $this->getIdAlumno());

				ResultadosPeer::addSelectColumns($criteria);
				$this->collResultadoss = ResultadosPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ResultadosPeer::ID_ALUMNO, $this->getIdAlumno());

				ResultadosPeer::addSelectColumns($criteria);
				if (!isset($this->lastResultadosCriteria) || !$this->lastResultadosCriteria->equals($criteria)) {
					$this->collResultadoss = ResultadosPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastResultadosCriteria = $criteria;
		return $this->collResultadoss;
	}

	
	public function countResultadoss($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ResultadosPeer::ID_ALUMNO, $this->getIdAlumno());

		return ResultadosPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addResultados(Resultados $l)
	{
		$this->collResultadoss[] = $l;
		$l->setAlumnos($this);
	}


	
	public function getResultadossJoinMuestras($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadoss === null) {
			if ($this->isNew()) {
				$this->collResultadoss = array();
			} else {

				$criteria->add(ResultadosPeer::ID_ALUMNO, $this->getIdAlumno());

				$this->collResultadoss = ResultadosPeer::doSelectJoinMuestras($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadosPeer::ID_ALUMNO, $this->getIdAlumno());

			if (!isset($this->lastResultadosCriteria) || !$this->lastResultadosCriteria->equals($criteria)) {
				$this->collResultadoss = ResultadosPeer::doSelectJoinMuestras($criteria, $con);
			}
		}
		$this->lastResultadosCriteria = $criteria;

		return $this->collResultadoss;
	}


	
	public function getResultadossJoinParametros($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadoss === null) {
			if ($this->isNew()) {
				$this->collResultadoss = array();
			} else {

				$criteria->add(ResultadosPeer::ID_ALUMNO, $this->getIdAlumno());

				$this->collResultadoss = ResultadosPeer::doSelectJoinParametros($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadosPeer::ID_ALUMNO, $this->getIdAlumno());

			if (!isset($this->lastResultadosCriteria) || !$this->lastResultadosCriteria->equals($criteria)) {
				$this->collResultadoss = ResultadosPeer::doSelectJoinParametros($criteria, $con);
			}
		}
		$this->lastResultadosCriteria = $criteria;

		return $this->collResultadoss;
	}

} 