<?php


abstract class BaseResultadojuez extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_resultadojuez;


	
	protected $id_juez;


	
	protected $id_muestra;


	
	protected $id_parametro;


	
	protected $resultado;

	
	protected $aJueces;

	
	protected $aMuestras;

	
	protected $aParametros;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdResultadojuez()
	{

		return $this->id_resultadojuez;
	}

	
	public function getIdJuez()
	{

		return $this->id_juez;
	}

	
	public function getIdMuestra()
	{

		return $this->id_muestra;
	}

	
	public function getIdParametro()
	{

		return $this->id_parametro;
	}

	
	public function getResultado()
	{

		return $this->resultado;
	}

	
	public function setIdResultadojuez($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_resultadojuez !== $v) {
			$this->id_resultadojuez = $v;
			$this->modifiedColumns[] = ResultadojuezPeer::ID_RESULTADOJUEZ;
		}

	} 
	
	public function setIdJuez($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_juez !== $v) {
			$this->id_juez = $v;
			$this->modifiedColumns[] = ResultadojuezPeer::ID_JUEZ;
		}

		if ($this->aJueces !== null && $this->aJueces->getIdJuez() !== $v) {
			$this->aJueces = null;
		}

	} 
	
	public function setIdMuestra($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_muestra !== $v) {
			$this->id_muestra = $v;
			$this->modifiedColumns[] = ResultadojuezPeer::ID_MUESTRA;
		}

		if ($this->aMuestras !== null && $this->aMuestras->getIdMuestra() !== $v) {
			$this->aMuestras = null;
		}

	} 
	
	public function setIdParametro($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_parametro !== $v) {
			$this->id_parametro = $v;
			$this->modifiedColumns[] = ResultadojuezPeer::ID_PARAMETRO;
		}

		if ($this->aParametros !== null && $this->aParametros->getIdParametro() !== $v) {
			$this->aParametros = null;
		}

	} 
	
	public function setResultado($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->resultado !== $v) {
			$this->resultado = $v;
			$this->modifiedColumns[] = ResultadojuezPeer::RESULTADO;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_resultadojuez = $rs->getInt($startcol + 0);

			$this->id_juez = $rs->getInt($startcol + 1);

			$this->id_muestra = $rs->getInt($startcol + 2);

			$this->id_parametro = $rs->getInt($startcol + 3);

			$this->resultado = $rs->getInt($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Resultadojuez object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ResultadojuezPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ResultadojuezPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ResultadojuezPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aJueces !== null) {
				if ($this->aJueces->isModified()) {
					$affectedRows += $this->aJueces->save($con);
				}
				$this->setJueces($this->aJueces);
			}

			if ($this->aMuestras !== null) {
				if ($this->aMuestras->isModified()) {
					$affectedRows += $this->aMuestras->save($con);
				}
				$this->setMuestras($this->aMuestras);
			}

			if ($this->aParametros !== null) {
				if ($this->aParametros->isModified()) {
					$affectedRows += $this->aParametros->save($con);
				}
				$this->setParametros($this->aParametros);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ResultadojuezPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdResultadojuez($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ResultadojuezPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aJueces !== null) {
				if (!$this->aJueces->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aJueces->getValidationFailures());
				}
			}

			if ($this->aMuestras !== null) {
				if (!$this->aMuestras->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aMuestras->getValidationFailures());
				}
			}

			if ($this->aParametros !== null) {
				if (!$this->aParametros->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aParametros->getValidationFailures());
				}
			}


			if (($retval = ResultadojuezPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ResultadojuezPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdResultadojuez();
				break;
			case 1:
				return $this->getIdJuez();
				break;
			case 2:
				return $this->getIdMuestra();
				break;
			case 3:
				return $this->getIdParametro();
				break;
			case 4:
				return $this->getResultado();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ResultadojuezPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdResultadojuez(),
			$keys[1] => $this->getIdJuez(),
			$keys[2] => $this->getIdMuestra(),
			$keys[3] => $this->getIdParametro(),
			$keys[4] => $this->getResultado(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ResultadojuezPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdResultadojuez($value);
				break;
			case 1:
				$this->setIdJuez($value);
				break;
			case 2:
				$this->setIdMuestra($value);
				break;
			case 3:
				$this->setIdParametro($value);
				break;
			case 4:
				$this->setResultado($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ResultadojuezPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdResultadojuez($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdJuez($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIdMuestra($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIdParametro($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setResultado($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ResultadojuezPeer::DATABASE_NAME);

		if ($this->isColumnModified(ResultadojuezPeer::ID_RESULTADOJUEZ)) $criteria->add(ResultadojuezPeer::ID_RESULTADOJUEZ, $this->id_resultadojuez);
		if ($this->isColumnModified(ResultadojuezPeer::ID_JUEZ)) $criteria->add(ResultadojuezPeer::ID_JUEZ, $this->id_juez);
		if ($this->isColumnModified(ResultadojuezPeer::ID_MUESTRA)) $criteria->add(ResultadojuezPeer::ID_MUESTRA, $this->id_muestra);
		if ($this->isColumnModified(ResultadojuezPeer::ID_PARAMETRO)) $criteria->add(ResultadojuezPeer::ID_PARAMETRO, $this->id_parametro);
		if ($this->isColumnModified(ResultadojuezPeer::RESULTADO)) $criteria->add(ResultadojuezPeer::RESULTADO, $this->resultado);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ResultadojuezPeer::DATABASE_NAME);

		$criteria->add(ResultadojuezPeer::ID_RESULTADOJUEZ, $this->id_resultadojuez);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdResultadojuez();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdResultadojuez($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdJuez($this->id_juez);

		$copyObj->setIdMuestra($this->id_muestra);

		$copyObj->setIdParametro($this->id_parametro);

		$copyObj->setResultado($this->resultado);


		$copyObj->setNew(true);

		$copyObj->setIdResultadojuez(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ResultadojuezPeer();
		}
		return self::$peer;
	}

	
	public function setJueces($v)
	{


		if ($v === null) {
			$this->setIdJuez(NULL);
		} else {
			$this->setIdJuez($v->getIdJuez());
		}


		$this->aJueces = $v;
	}


	
	public function getJueces($con = null)
	{
				include_once 'lib/model/om/BaseJuecesPeer.php';

		if ($this->aJueces === null && ($this->id_juez !== null)) {

			$this->aJueces = JuecesPeer::retrieveByPK($this->id_juez, $con);

			
		}
		return $this->aJueces;
	}

	
	public function setMuestras($v)
	{


		if ($v === null) {
			$this->setIdMuestra(NULL);
		} else {
			$this->setIdMuestra($v->getIdMuestra());
		}


		$this->aMuestras = $v;
	}


	
	public function getMuestras($con = null)
	{
				include_once 'lib/model/om/BaseMuestrasPeer.php';

		if ($this->aMuestras === null && ($this->id_muestra !== null)) {

			$this->aMuestras = MuestrasPeer::retrieveByPK($this->id_muestra, $con);

			
		}
		return $this->aMuestras;
	}

	
	public function setParametros($v)
	{


		if ($v === null) {
			$this->setIdParametro(NULL);
		} else {
			$this->setIdParametro($v->getIdParametro());
		}


		$this->aParametros = $v;
	}


	
	public function getParametros($con = null)
	{
				include_once 'lib/model/om/BaseParametrosPeer.php';

		if ($this->aParametros === null && ($this->id_parametro !== null)) {

			$this->aParametros = ParametrosPeer::retrieveByPK($this->id_parametro, $con);

			
		}
		return $this->aParametros;
	}

} 