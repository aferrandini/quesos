<?php


abstract class BaseParametros extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_parametro;


	
	protected $id_tipomuestra;


	
	protected $nombre;


	
	protected $id_fase;


	
	protected $id_separador;

	
	protected $aTipomuestra;

	
	protected $aFases;

	
	protected $aSeparadores;

	
	protected $collResultadojuezs;

	
	protected $lastResultadojuezCriteria = null;

	
	protected $collResultadoss;

	
	protected $lastResultadosCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdParametro()
	{

		return $this->id_parametro;
	}

	
	public function getIdTipomuestra()
	{

		return $this->id_tipomuestra;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function getIdFase()
	{

		return $this->id_fase;
	}

	
	public function getIdSeparador()
	{

		return $this->id_separador;
	}

	
	public function setIdParametro($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_parametro !== $v) {
			$this->id_parametro = $v;
			$this->modifiedColumns[] = ParametrosPeer::ID_PARAMETRO;
		}

	} 
	
	public function setIdTipomuestra($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_tipomuestra !== $v) {
			$this->id_tipomuestra = $v;
			$this->modifiedColumns[] = ParametrosPeer::ID_TIPOMUESTRA;
		}

		if ($this->aTipomuestra !== null && $this->aTipomuestra->getIdTipomuestra() !== $v) {
			$this->aTipomuestra = null;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = ParametrosPeer::NOMBRE;
		}

	} 
	
	public function setIdFase($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_fase !== $v) {
			$this->id_fase = $v;
			$this->modifiedColumns[] = ParametrosPeer::ID_FASE;
		}

		if ($this->aFases !== null && $this->aFases->getIdFase() !== $v) {
			$this->aFases = null;
		}

	} 
	
	public function setIdSeparador($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_separador !== $v) {
			$this->id_separador = $v;
			$this->modifiedColumns[] = ParametrosPeer::ID_SEPARADOR;
		}

		if ($this->aSeparadores !== null && $this->aSeparadores->getIdSeparador() !== $v) {
			$this->aSeparadores = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_parametro = $rs->getInt($startcol + 0);

			$this->id_tipomuestra = $rs->getInt($startcol + 1);

			$this->nombre = $rs->getString($startcol + 2);

			$this->id_fase = $rs->getInt($startcol + 3);

			$this->id_separador = $rs->getInt($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Parametros object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ParametrosPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ParametrosPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ParametrosPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aTipomuestra !== null) {
				if ($this->aTipomuestra->isModified()) {
					$affectedRows += $this->aTipomuestra->save($con);
				}
				$this->setTipomuestra($this->aTipomuestra);
			}

			if ($this->aFases !== null) {
				if ($this->aFases->isModified()) {
					$affectedRows += $this->aFases->save($con);
				}
				$this->setFases($this->aFases);
			}

			if ($this->aSeparadores !== null) {
				if ($this->aSeparadores->isModified()) {
					$affectedRows += $this->aSeparadores->save($con);
				}
				$this->setSeparadores($this->aSeparadores);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ParametrosPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdParametro($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ParametrosPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collResultadojuezs !== null) {
				foreach($this->collResultadojuezs as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collResultadoss !== null) {
				foreach($this->collResultadoss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aTipomuestra !== null) {
				if (!$this->aTipomuestra->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aTipomuestra->getValidationFailures());
				}
			}

			if ($this->aFases !== null) {
				if (!$this->aFases->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aFases->getValidationFailures());
				}
			}

			if ($this->aSeparadores !== null) {
				if (!$this->aSeparadores->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aSeparadores->getValidationFailures());
				}
			}


			if (($retval = ParametrosPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collResultadojuezs !== null) {
					foreach($this->collResultadojuezs as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collResultadoss !== null) {
					foreach($this->collResultadoss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ParametrosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdParametro();
				break;
			case 1:
				return $this->getIdTipomuestra();
				break;
			case 2:
				return $this->getNombre();
				break;
			case 3:
				return $this->getIdFase();
				break;
			case 4:
				return $this->getIdSeparador();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ParametrosPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdParametro(),
			$keys[1] => $this->getIdTipomuestra(),
			$keys[2] => $this->getNombre(),
			$keys[3] => $this->getIdFase(),
			$keys[4] => $this->getIdSeparador(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ParametrosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdParametro($value);
				break;
			case 1:
				$this->setIdTipomuestra($value);
				break;
			case 2:
				$this->setNombre($value);
				break;
			case 3:
				$this->setIdFase($value);
				break;
			case 4:
				$this->setIdSeparador($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ParametrosPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdParametro($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdTipomuestra($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNombre($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIdFase($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIdSeparador($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ParametrosPeer::DATABASE_NAME);

		if ($this->isColumnModified(ParametrosPeer::ID_PARAMETRO)) $criteria->add(ParametrosPeer::ID_PARAMETRO, $this->id_parametro);
		if ($this->isColumnModified(ParametrosPeer::ID_TIPOMUESTRA)) $criteria->add(ParametrosPeer::ID_TIPOMUESTRA, $this->id_tipomuestra);
		if ($this->isColumnModified(ParametrosPeer::NOMBRE)) $criteria->add(ParametrosPeer::NOMBRE, $this->nombre);
		if ($this->isColumnModified(ParametrosPeer::ID_FASE)) $criteria->add(ParametrosPeer::ID_FASE, $this->id_fase);
		if ($this->isColumnModified(ParametrosPeer::ID_SEPARADOR)) $criteria->add(ParametrosPeer::ID_SEPARADOR, $this->id_separador);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ParametrosPeer::DATABASE_NAME);

		$criteria->add(ParametrosPeer::ID_PARAMETRO, $this->id_parametro);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdParametro();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdParametro($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdTipomuestra($this->id_tipomuestra);

		$copyObj->setNombre($this->nombre);

		$copyObj->setIdFase($this->id_fase);

		$copyObj->setIdSeparador($this->id_separador);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getResultadojuezs() as $relObj) {
				$copyObj->addResultadojuez($relObj->copy($deepCopy));
			}

			foreach($this->getResultadoss() as $relObj) {
				$copyObj->addResultados($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setIdParametro(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ParametrosPeer();
		}
		return self::$peer;
	}

	
	public function setTipomuestra($v)
	{


		if ($v === null) {
			$this->setIdTipomuestra(NULL);
		} else {
			$this->setIdTipomuestra($v->getIdTipomuestra());
		}


		$this->aTipomuestra = $v;
	}


	
	public function getTipomuestra($con = null)
	{
				include_once 'lib/model/om/BaseTipomuestraPeer.php';

		if ($this->aTipomuestra === null && ($this->id_tipomuestra !== null)) {

			$this->aTipomuestra = TipomuestraPeer::retrieveByPK($this->id_tipomuestra, $con);

			
		}
		return $this->aTipomuestra;
	}

	
	public function setFases($v)
	{


		if ($v === null) {
			$this->setIdFase(NULL);
		} else {
			$this->setIdFase($v->getIdFase());
		}


		$this->aFases = $v;
	}


	
	public function getFases($con = null)
	{
				include_once 'lib/model/om/BaseFasesPeer.php';

		if ($this->aFases === null && ($this->id_fase !== null)) {

			$this->aFases = FasesPeer::retrieveByPK($this->id_fase, $con);

			
		}
		return $this->aFases;
	}

	
	public function setSeparadores($v)
	{


		if ($v === null) {
			$this->setIdSeparador(NULL);
		} else {
			$this->setIdSeparador($v->getIdSeparador());
		}


		$this->aSeparadores = $v;
	}


	
	public function getSeparadores($con = null)
	{
				include_once 'lib/model/om/BaseSeparadoresPeer.php';

		if ($this->aSeparadores === null && ($this->id_separador !== null)) {

			$this->aSeparadores = SeparadoresPeer::retrieveByPK($this->id_separador, $con);

			
		}
		return $this->aSeparadores;
	}

	
	public function initResultadojuezs()
	{
		if ($this->collResultadojuezs === null) {
			$this->collResultadojuezs = array();
		}
	}

	
	public function getResultadojuezs($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadojuezs === null) {
			if ($this->isNew()) {
			   $this->collResultadojuezs = array();
			} else {

				$criteria->add(ResultadojuezPeer::ID_PARAMETRO, $this->getIdParametro());

				ResultadojuezPeer::addSelectColumns($criteria);
				$this->collResultadojuezs = ResultadojuezPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ResultadojuezPeer::ID_PARAMETRO, $this->getIdParametro());

				ResultadojuezPeer::addSelectColumns($criteria);
				if (!isset($this->lastResultadojuezCriteria) || !$this->lastResultadojuezCriteria->equals($criteria)) {
					$this->collResultadojuezs = ResultadojuezPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastResultadojuezCriteria = $criteria;
		return $this->collResultadojuezs;
	}

	
	public function countResultadojuezs($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ResultadojuezPeer::ID_PARAMETRO, $this->getIdParametro());

		return ResultadojuezPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addResultadojuez(Resultadojuez $l)
	{
		$this->collResultadojuezs[] = $l;
		$l->setParametros($this);
	}


	
	public function getResultadojuezsJoinJueces($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadojuezs === null) {
			if ($this->isNew()) {
				$this->collResultadojuezs = array();
			} else {

				$criteria->add(ResultadojuezPeer::ID_PARAMETRO, $this->getIdParametro());

				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinJueces($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadojuezPeer::ID_PARAMETRO, $this->getIdParametro());

			if (!isset($this->lastResultadojuezCriteria) || !$this->lastResultadojuezCriteria->equals($criteria)) {
				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinJueces($criteria, $con);
			}
		}
		$this->lastResultadojuezCriteria = $criteria;

		return $this->collResultadojuezs;
	}


	
	public function getResultadojuezsJoinMuestras($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadojuezPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadojuezs === null) {
			if ($this->isNew()) {
				$this->collResultadojuezs = array();
			} else {

				$criteria->add(ResultadojuezPeer::ID_PARAMETRO, $this->getIdParametro());

				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinMuestras($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadojuezPeer::ID_PARAMETRO, $this->getIdParametro());

			if (!isset($this->lastResultadojuezCriteria) || !$this->lastResultadojuezCriteria->equals($criteria)) {
				$this->collResultadojuezs = ResultadojuezPeer::doSelectJoinMuestras($criteria, $con);
			}
		}
		$this->lastResultadojuezCriteria = $criteria;

		return $this->collResultadojuezs;
	}

	
	public function initResultadoss()
	{
		if ($this->collResultadoss === null) {
			$this->collResultadoss = array();
		}
	}

	
	public function getResultadoss($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadoss === null) {
			if ($this->isNew()) {
			   $this->collResultadoss = array();
			} else {

				$criteria->add(ResultadosPeer::ID_PARAMETRO, $this->getIdParametro());

				ResultadosPeer::addSelectColumns($criteria);
				$this->collResultadoss = ResultadosPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(ResultadosPeer::ID_PARAMETRO, $this->getIdParametro());

				ResultadosPeer::addSelectColumns($criteria);
				if (!isset($this->lastResultadosCriteria) || !$this->lastResultadosCriteria->equals($criteria)) {
					$this->collResultadoss = ResultadosPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastResultadosCriteria = $criteria;
		return $this->collResultadoss;
	}

	
	public function countResultadoss($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(ResultadosPeer::ID_PARAMETRO, $this->getIdParametro());

		return ResultadosPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addResultados(Resultados $l)
	{
		$this->collResultadoss[] = $l;
		$l->setParametros($this);
	}


	
	public function getResultadossJoinAlumnos($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadoss === null) {
			if ($this->isNew()) {
				$this->collResultadoss = array();
			} else {

				$criteria->add(ResultadosPeer::ID_PARAMETRO, $this->getIdParametro());

				$this->collResultadoss = ResultadosPeer::doSelectJoinAlumnos($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadosPeer::ID_PARAMETRO, $this->getIdParametro());

			if (!isset($this->lastResultadosCriteria) || !$this->lastResultadosCriteria->equals($criteria)) {
				$this->collResultadoss = ResultadosPeer::doSelectJoinAlumnos($criteria, $con);
			}
		}
		$this->lastResultadosCriteria = $criteria;

		return $this->collResultadoss;
	}


	
	public function getResultadossJoinMuestras($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseResultadosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collResultadoss === null) {
			if ($this->isNew()) {
				$this->collResultadoss = array();
			} else {

				$criteria->add(ResultadosPeer::ID_PARAMETRO, $this->getIdParametro());

				$this->collResultadoss = ResultadosPeer::doSelectJoinMuestras($criteria, $con);
			}
		} else {
									
			$criteria->add(ResultadosPeer::ID_PARAMETRO, $this->getIdParametro());

			if (!isset($this->lastResultadosCriteria) || !$this->lastResultadosCriteria->equals($criteria)) {
				$this->collResultadoss = ResultadosPeer::doSelectJoinMuestras($criteria, $con);
			}
		}
		$this->lastResultadosCriteria = $criteria;

		return $this->collResultadoss;
	}

} 