<?php


abstract class BaseParametrosPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'parametros';

	
	const CLASS_DEFAULT = 'lib.model.Parametros';

	
	const NUM_COLUMNS = 5;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_PARAMETRO = 'parametros.ID_PARAMETRO';

	
	const ID_TIPOMUESTRA = 'parametros.ID_TIPOMUESTRA';

	
	const NOMBRE = 'parametros.NOMBRE';

	
	const ID_FASE = 'parametros.ID_FASE';

	
	const ID_SEPARADOR = 'parametros.ID_SEPARADOR';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdParametro', 'IdTipomuestra', 'Nombre', 'IdFase', 'IdSeparador', ),
		BasePeer::TYPE_COLNAME => array (ParametrosPeer::ID_PARAMETRO, ParametrosPeer::ID_TIPOMUESTRA, ParametrosPeer::NOMBRE, ParametrosPeer::ID_FASE, ParametrosPeer::ID_SEPARADOR, ),
		BasePeer::TYPE_FIELDNAME => array ('id_parametro', 'id_tipomuestra', 'nombre', 'id_fase', 'id_separador', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdParametro' => 0, 'IdTipomuestra' => 1, 'Nombre' => 2, 'IdFase' => 3, 'IdSeparador' => 4, ),
		BasePeer::TYPE_COLNAME => array (ParametrosPeer::ID_PARAMETRO => 0, ParametrosPeer::ID_TIPOMUESTRA => 1, ParametrosPeer::NOMBRE => 2, ParametrosPeer::ID_FASE => 3, ParametrosPeer::ID_SEPARADOR => 4, ),
		BasePeer::TYPE_FIELDNAME => array ('id_parametro' => 0, 'id_tipomuestra' => 1, 'nombre' => 2, 'id_fase' => 3, 'id_separador' => 4, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/ParametrosMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.ParametrosMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ParametrosPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ParametrosPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ParametrosPeer::ID_PARAMETRO);

		$criteria->addSelectColumn(ParametrosPeer::ID_TIPOMUESTRA);

		$criteria->addSelectColumn(ParametrosPeer::NOMBRE);

		$criteria->addSelectColumn(ParametrosPeer::ID_FASE);

		$criteria->addSelectColumn(ParametrosPeer::ID_SEPARADOR);

	}

	const COUNT = 'COUNT(parametros.ID_PARAMETRO)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT parametros.ID_PARAMETRO)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ParametrosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ParametrosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ParametrosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ParametrosPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ParametrosPeer::populateObjects(ParametrosPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ParametrosPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ParametrosPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinTipomuestra(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ParametrosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ParametrosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ParametrosPeer::ID_TIPOMUESTRA, TipomuestraPeer::ID_TIPOMUESTRA);

		$rs = ParametrosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinFases(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ParametrosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ParametrosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ParametrosPeer::ID_FASE, FasesPeer::ID_FASE);

		$rs = ParametrosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinSeparadores(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ParametrosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ParametrosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ParametrosPeer::ID_SEPARADOR, SeparadoresPeer::ID_SEPARADOR);

		$rs = ParametrosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinTipomuestra(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ParametrosPeer::addSelectColumns($c);
		$startcol = (ParametrosPeer::NUM_COLUMNS - ParametrosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		TipomuestraPeer::addSelectColumns($c);

		$c->addJoin(ParametrosPeer::ID_TIPOMUESTRA, TipomuestraPeer::ID_TIPOMUESTRA);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ParametrosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TipomuestraPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getTipomuestra(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addParametros($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initParametross();
				$obj2->addParametros($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinFases(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ParametrosPeer::addSelectColumns($c);
		$startcol = (ParametrosPeer::NUM_COLUMNS - ParametrosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		FasesPeer::addSelectColumns($c);

		$c->addJoin(ParametrosPeer::ID_FASE, FasesPeer::ID_FASE);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ParametrosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = FasesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getFases(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addParametros($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initParametross();
				$obj2->addParametros($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinSeparadores(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ParametrosPeer::addSelectColumns($c);
		$startcol = (ParametrosPeer::NUM_COLUMNS - ParametrosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		SeparadoresPeer::addSelectColumns($c);

		$c->addJoin(ParametrosPeer::ID_SEPARADOR, SeparadoresPeer::ID_SEPARADOR);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ParametrosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = SeparadoresPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getSeparadores(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addParametros($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initParametross();
				$obj2->addParametros($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ParametrosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ParametrosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ParametrosPeer::ID_TIPOMUESTRA, TipomuestraPeer::ID_TIPOMUESTRA);

		$criteria->addJoin(ParametrosPeer::ID_FASE, FasesPeer::ID_FASE);

		$criteria->addJoin(ParametrosPeer::ID_SEPARADOR, SeparadoresPeer::ID_SEPARADOR);

		$rs = ParametrosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ParametrosPeer::addSelectColumns($c);
		$startcol2 = (ParametrosPeer::NUM_COLUMNS - ParametrosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TipomuestraPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TipomuestraPeer::NUM_COLUMNS;

		FasesPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + FasesPeer::NUM_COLUMNS;

		SeparadoresPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + SeparadoresPeer::NUM_COLUMNS;

		$c->addJoin(ParametrosPeer::ID_TIPOMUESTRA, TipomuestraPeer::ID_TIPOMUESTRA);

		$c->addJoin(ParametrosPeer::ID_FASE, FasesPeer::ID_FASE);

		$c->addJoin(ParametrosPeer::ID_SEPARADOR, SeparadoresPeer::ID_SEPARADOR);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ParametrosPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = TipomuestraPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTipomuestra(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addParametros($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initParametross();
				$obj2->addParametros($obj1);
			}


					
			$omClass = FasesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getFases(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addParametros($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initParametross();
				$obj3->addParametros($obj1);
			}


					
			$omClass = SeparadoresPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getSeparadores(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addParametros($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj4->initParametross();
				$obj4->addParametros($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptTipomuestra(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ParametrosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ParametrosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ParametrosPeer::ID_FASE, FasesPeer::ID_FASE);

		$criteria->addJoin(ParametrosPeer::ID_SEPARADOR, SeparadoresPeer::ID_SEPARADOR);

		$rs = ParametrosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptFases(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ParametrosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ParametrosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ParametrosPeer::ID_TIPOMUESTRA, TipomuestraPeer::ID_TIPOMUESTRA);

		$criteria->addJoin(ParametrosPeer::ID_SEPARADOR, SeparadoresPeer::ID_SEPARADOR);

		$rs = ParametrosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptSeparadores(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ParametrosPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ParametrosPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(ParametrosPeer::ID_TIPOMUESTRA, TipomuestraPeer::ID_TIPOMUESTRA);

		$criteria->addJoin(ParametrosPeer::ID_FASE, FasesPeer::ID_FASE);

		$rs = ParametrosPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptTipomuestra(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ParametrosPeer::addSelectColumns($c);
		$startcol2 = (ParametrosPeer::NUM_COLUMNS - ParametrosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		FasesPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + FasesPeer::NUM_COLUMNS;

		SeparadoresPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + SeparadoresPeer::NUM_COLUMNS;

		$c->addJoin(ParametrosPeer::ID_FASE, FasesPeer::ID_FASE);

		$c->addJoin(ParametrosPeer::ID_SEPARADOR, SeparadoresPeer::ID_SEPARADOR);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ParametrosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = FasesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getFases(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addParametros($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initParametross();
				$obj2->addParametros($obj1);
			}

			$omClass = SeparadoresPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getSeparadores(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addParametros($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initParametross();
				$obj3->addParametros($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptFases(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ParametrosPeer::addSelectColumns($c);
		$startcol2 = (ParametrosPeer::NUM_COLUMNS - ParametrosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TipomuestraPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TipomuestraPeer::NUM_COLUMNS;

		SeparadoresPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + SeparadoresPeer::NUM_COLUMNS;

		$c->addJoin(ParametrosPeer::ID_TIPOMUESTRA, TipomuestraPeer::ID_TIPOMUESTRA);

		$c->addJoin(ParametrosPeer::ID_SEPARADOR, SeparadoresPeer::ID_SEPARADOR);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ParametrosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TipomuestraPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTipomuestra(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addParametros($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initParametross();
				$obj2->addParametros($obj1);
			}

			$omClass = SeparadoresPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getSeparadores(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addParametros($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initParametross();
				$obj3->addParametros($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptSeparadores(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		ParametrosPeer::addSelectColumns($c);
		$startcol2 = (ParametrosPeer::NUM_COLUMNS - ParametrosPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		TipomuestraPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + TipomuestraPeer::NUM_COLUMNS;

		FasesPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + FasesPeer::NUM_COLUMNS;

		$c->addJoin(ParametrosPeer::ID_TIPOMUESTRA, TipomuestraPeer::ID_TIPOMUESTRA);

		$c->addJoin(ParametrosPeer::ID_FASE, FasesPeer::ID_FASE);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = ParametrosPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = TipomuestraPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getTipomuestra(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addParametros($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initParametross();
				$obj2->addParametros($obj1);
			}

			$omClass = FasesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getFases(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addParametros($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initParametross();
				$obj3->addParametros($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ParametrosPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(ParametrosPeer::ID_PARAMETRO); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ParametrosPeer::ID_PARAMETRO);
			$selectCriteria->add(ParametrosPeer::ID_PARAMETRO, $criteria->remove(ParametrosPeer::ID_PARAMETRO), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += ParametrosPeer::doOnDeleteCascade(new Criteria(), $con);
			$affectedRows += BasePeer::doDeleteAll(ParametrosPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ParametrosPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Parametros) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ParametrosPeer::ID_PARAMETRO, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			$affectedRows += ParametrosPeer::doOnDeleteCascade($criteria, $con);
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected static function doOnDeleteCascade(Criteria $criteria, Connection $con)
	{
				$affectedRows = 0;

				$objects = ParametrosPeer::doSelect($criteria, $con);
		foreach($objects as $obj) {


			include_once 'lib/model/Resultadojuez.php';

						$c = new Criteria();
			
			$c->add(ResultadojuezPeer::ID_PARAMETRO, $obj->getIdParametro());
			$affectedRows += ResultadojuezPeer::doDelete($c, $con);

			include_once 'lib/model/Resultados.php';

						$c = new Criteria();
			
			$c->add(ResultadosPeer::ID_PARAMETRO, $obj->getIdParametro());
			$affectedRows += ResultadosPeer::doDelete($c, $con);
		}
		return $affectedRows;
	}

	
	public static function doValidate(Parametros $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ParametrosPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ParametrosPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ParametrosPeer::DATABASE_NAME, ParametrosPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ParametrosPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ParametrosPeer::DATABASE_NAME);

		$criteria->add(ParametrosPeer::ID_PARAMETRO, $pk);


		$v = ParametrosPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ParametrosPeer::ID_PARAMETRO, $pks, Criteria::IN);
			$objs = ParametrosPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseParametrosPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/ParametrosMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.ParametrosMapBuilder');
}
