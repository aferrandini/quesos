<?php


abstract class BaseEquipos extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_equipo;


	
	protected $nombre;


	
	protected $profesor;


	
	protected $id_pais;

	
	protected $aPaises;

	
	protected $collAlumnoss;

	
	protected $lastAlumnosCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdEquipo()
	{

		return $this->id_equipo;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function getProfesor()
	{

		return $this->profesor;
	}

	
	public function getIdPais()
	{

		return $this->id_pais;
	}

	
	public function setIdEquipo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_equipo !== $v) {
			$this->id_equipo = $v;
			$this->modifiedColumns[] = EquiposPeer::ID_EQUIPO;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = EquiposPeer::NOMBRE;
		}

	} 
	
	public function setProfesor($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->profesor !== $v) {
			$this->profesor = $v;
			$this->modifiedColumns[] = EquiposPeer::PROFESOR;
		}

	} 
	
	public function setIdPais($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_pais !== $v) {
			$this->id_pais = $v;
			$this->modifiedColumns[] = EquiposPeer::ID_PAIS;
		}

		if ($this->aPaises !== null && $this->aPaises->getIdPais() !== $v) {
			$this->aPaises = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_equipo = $rs->getInt($startcol + 0);

			$this->nombre = $rs->getString($startcol + 1);

			$this->profesor = $rs->getString($startcol + 2);

			$this->id_pais = $rs->getInt($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Equipos object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(EquiposPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			EquiposPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(EquiposPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aPaises !== null) {
				if ($this->aPaises->isModified()) {
					$affectedRows += $this->aPaises->save($con);
				}
				$this->setPaises($this->aPaises);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = EquiposPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdEquipo($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += EquiposPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collAlumnoss !== null) {
				foreach($this->collAlumnoss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aPaises !== null) {
				if (!$this->aPaises->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aPaises->getValidationFailures());
				}
			}


			if (($retval = EquiposPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collAlumnoss !== null) {
					foreach($this->collAlumnoss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = EquiposPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdEquipo();
				break;
			case 1:
				return $this->getNombre();
				break;
			case 2:
				return $this->getProfesor();
				break;
			case 3:
				return $this->getIdPais();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = EquiposPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdEquipo(),
			$keys[1] => $this->getNombre(),
			$keys[2] => $this->getProfesor(),
			$keys[3] => $this->getIdPais(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = EquiposPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdEquipo($value);
				break;
			case 1:
				$this->setNombre($value);
				break;
			case 2:
				$this->setProfesor($value);
				break;
			case 3:
				$this->setIdPais($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = EquiposPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdEquipo($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setProfesor($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIdPais($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(EquiposPeer::DATABASE_NAME);

		if ($this->isColumnModified(EquiposPeer::ID_EQUIPO)) $criteria->add(EquiposPeer::ID_EQUIPO, $this->id_equipo);
		if ($this->isColumnModified(EquiposPeer::NOMBRE)) $criteria->add(EquiposPeer::NOMBRE, $this->nombre);
		if ($this->isColumnModified(EquiposPeer::PROFESOR)) $criteria->add(EquiposPeer::PROFESOR, $this->profesor);
		if ($this->isColumnModified(EquiposPeer::ID_PAIS)) $criteria->add(EquiposPeer::ID_PAIS, $this->id_pais);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(EquiposPeer::DATABASE_NAME);

		$criteria->add(EquiposPeer::ID_EQUIPO, $this->id_equipo);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdEquipo();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdEquipo($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNombre($this->nombre);

		$copyObj->setProfesor($this->profesor);

		$copyObj->setIdPais($this->id_pais);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getAlumnoss() as $relObj) {
				$copyObj->addAlumnos($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setIdEquipo(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new EquiposPeer();
		}
		return self::$peer;
	}

	
	public function setPaises($v)
	{


		if ($v === null) {
			$this->setIdPais(NULL);
		} else {
			$this->setIdPais($v->getIdPais());
		}


		$this->aPaises = $v;
	}


	
	public function getPaises($con = null)
	{
				include_once 'lib/model/om/BasePaisesPeer.php';

		if ($this->aPaises === null && ($this->id_pais !== null)) {

			$this->aPaises = PaisesPeer::retrieveByPK($this->id_pais, $con);

			
		}
		return $this->aPaises;
	}

	
	public function initAlumnoss()
	{
		if ($this->collAlumnoss === null) {
			$this->collAlumnoss = array();
		}
	}

	
	public function getAlumnoss($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseAlumnosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collAlumnoss === null) {
			if ($this->isNew()) {
			   $this->collAlumnoss = array();
			} else {

				$criteria->add(AlumnosPeer::ID_EQUIPO, $this->getIdEquipo());

				AlumnosPeer::addSelectColumns($criteria);
				$this->collAlumnoss = AlumnosPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(AlumnosPeer::ID_EQUIPO, $this->getIdEquipo());

				AlumnosPeer::addSelectColumns($criteria);
				if (!isset($this->lastAlumnosCriteria) || !$this->lastAlumnosCriteria->equals($criteria)) {
					$this->collAlumnoss = AlumnosPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastAlumnosCriteria = $criteria;
		return $this->collAlumnoss;
	}

	
	public function countAlumnoss($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseAlumnosPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(AlumnosPeer::ID_EQUIPO, $this->getIdEquipo());

		return AlumnosPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addAlumnos(Alumnos $l)
	{
		$this->collAlumnoss[] = $l;
		$l->setEquipos($this);
	}

} 