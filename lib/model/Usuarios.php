<?php

/**
 * Subclass for representing a row from the 'usuarios' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Usuarios extends BaseUsuarios
{
  public function getNadmin() {
    if($this->getAdmin()) {
      return image_tag('/sf/sf_admin/images/ok.png',array('width'=>'16','height'=>'16'));
    } else {
      return image_tag('/sf/sf_admin/images/cancel.png',array('width'=>'16','height'=>'16'));
    }
  }
}
