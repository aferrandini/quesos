<?php

/**
 * Subclass for representing a row from the 'paises' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Paises extends BasePaises
{
  public function __toString() {
    return $this->getPais();
  }

  public function getPais() {
    return sfContext::getInstance()->getI18N()->__(parent::getPais());
  }
  
  public function getFlag() {
    if (file_exists(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'flags'.DIRECTORY_SEPARATOR.$this->getCodigo().'.gif')) {
    	return image_tag('/images/flags/'.$this->getCodigo().'.gif');
    } else {
      return image_tag('/images/flags/-.gif');
    }
  }
}
