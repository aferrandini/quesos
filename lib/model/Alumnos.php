<?php

/**
 * Subclass for representing a row from the 'alumnos' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Alumnos extends BaseAlumnos
{
  public function __toString() {
    return $this->getNombre();
  }
  
  public function getFlag() {
    if($this->getEquipos() instanceof Equipos ) {
      return $this->getEquipos()->getFlag();  
    } else {
      $p = new Paises();
      return $p->getFlag();
    }
  }
  
  public function getDatosintroducidos() {
    $total_muestras = MuestrasPeer::doCount(new Criteria());
    
    $sql = "SELECT COUNT(*) FROM calculos WHERE id_alumno='".$this->getPrimaryKey()."' GROUP BY id_muestra";
    $con = Propel::getConnection();
    $stmt = $con->createStatement();
    $res = $stmt->executeQuery($sql, ResultSet::FETCHMODE_NUM );
    
    $total = 0;
    while ($res->next()) {
    	$total++;
    }
    
    $texto = '<table border="0" cellpadding="0" cellspacing="0">'
      .'<tr>'
      .'<td style="width: 40px; text-align: right; border: 0px">['.round((($total/$total_muestras)*100)).'%]</td>'
      .'<td style="width: 50px; border: 0px">';
    for ($i=1;$i<=$total;$i++) {
      $texto .= '|';
    }
    $texto .= '</td>'
      .'<td style="width: 40px; text-align: right; border: 0px">'.$total.' / '.$total_muestras.'</td></tr></table>';
    
    return $texto;
  }
}
