<?php

/**
 * Subclass for representing a row from the 'muestras' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Muestras extends BaseMuestras
{
  public function getResultado($id_alumno) {
    $sql = "SELECT SUM(resultado) FROM calculos WHERE id_alumno='$id_alumno' AND id_muestra='".$this->getPrimaryKey()."' GROUP BY id_muestra";
    $con = Propel::getConnection();
    $stmt = $con->createStatement();
    $res = $stmt->executeQuery($sql, ResultSet::FETCHMODE_NUM );
    
    while ($res->next()) {
    	return $res->get(1);
    }
    return 0;
  }
  
  public function getNombre() {
    $tmuestra = $this->getTipomuestra();
    $c = new Criteria();
    $c->clearSelectColumns()->addSelectColumn(MuestrasPeer::ID_MUESTRA );
    $c->add(MuestrasPeer::ID_TIPO , $tmuestra->getPrimaryKey());
    $mRS = MuestrasPeer::doSelectRS($c);
    $i = 0;
    while ($mRS->next()) {
    	$i++;
    	
    	if($mRS->get(1)==$this->getPrimaryKey()) break;
    }
    
    return $tmuestra->getNombre().' '.$i;
  }
}
