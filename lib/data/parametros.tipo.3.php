<?php
/**
 * array parametros tipo muestra 3
 */
$arrDataParametros = array();
$arrDataParametros[] = array("nombre" => "Descriptor", "id_fase" => "1", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Marca de los pa�os", "id_fase" => "1", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Forma piriforme", "id_fase" => "1", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Zona de corteza", "id_fase" => "1", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Pasta brillante", "id_fase" => "1", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Número de ojos", "id_fase" => "1", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Tamaño de los ojos", "id_fase" => "1", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Distribución de ojos", "id_fase" => "1", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Color de la pasta", "id_fase" => "1", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Descriptor", "id_fase" => "2", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Rugosidad", "id_fase" => "2", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Humedad", "id_fase" => "2", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Elasticidad", "id_fase" => "2", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Descriptor", "id_fase" => "3", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Olor a leche de vaca", "id_fase" => "3", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "A leche cocida", "id_fase" => "3", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "A leche acidificada", "id_fase" => "3", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "A mantequilla", "id_fase" => "3", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "A mohoso", "id_fase" => "3", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Descriptor", "id_fase" => "4", "id_separador" => "");
$arrDataParametros[] = array("nombre" => "Firmeza", "id_fase" => "4", "id_separador" => "1");
$arrDataParametros[] = array("nombre" => "Friabilidad", "id_fase" => "4", "id_separador" => "1");
$arrDataParametros[] = array("nombre" => "Adherencia", "id_fase" => "4", "id_separador" => "1");
$arrDataParametros[] = array("nombre" => "Granulosidad", "id_fase" => "4", "id_separador" => "1");
$arrDataParametros[] = array("nombre" => "Humedad", "id_fase" => "4", "id_separador" => "1");
$arrDataParametros[] = array("nombre" => "Car�cter graso", "id_fase" => "4", "id_separador" => "1");
$arrDataParametros[] = array("nombre" => "Fundente", "id_fase" => "4", "id_separador" => "1");
$arrDataParametros[] = array("nombre" => "Aroma a leche cocida", "id_fase" => "4", "id_separador" => "2");
$arrDataParametros[] = array("nombre" => "Aroma a mantequilla", "id_fase" => "4", "id_separador" => "2");
$arrDataParametros[] = array("nombre" => "Aroma a rancio", "id_fase" => "4", "id_separador" => "2");
$arrDataParametros[] = array("nombre" => "Sabor �cido", "id_fase" => "4", "id_separador" => "2");
$arrDataParametros[] = array("nombre" => "Salado", "id_fase" => "4", "id_separador" => "2");
$arrDataParametros[] = array("nombre" => "Amargo", "id_fase" => "4", "id_separador" => "2");
$arrDataParametros[] = array("nombre" => "Met�lico", "id_fase" => "4", "id_separador" => "2");
$arrDataParametros[] = array("nombre" => "Gusto residual a jab�n", "id_fase" => "4", "id_separador" => "2");