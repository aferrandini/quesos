<?php

class pdflistadoparticipantes {
  var $pdf;

  public function __construct() {
    /**
     * Generamos el PDf
     */
    $this->pdf = new Cezpdf('a4','portrait');
  }

  public function stream() {
    /**
     * Generamos el informe y se lo damos a quien llam� a la funci�n
     */
    $this->pdf->selectFont(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'/fonts/Helvetica.afm');
    $this->pdf->ezSetCmMargins(1.8,2,2,2);

    /**
     * Resultados general por participantes
     */
    $this->pdf->setColor(0,0,0.5);
    $this->pdf->filledRectangle(0,770,600,50);

    $this->salto(-25);

    $this->pdf->setColor(1,1,1);
    $this->pdf->ezText('<b>'.__('Resultado general').'</b>',14,array());

    $this->salto(10);

    $fecha = date('d/m/Y H:i:s');
    $this->pdf->ezText($fecha, 10);
    
    $this->pdf->setColor(0,0,0);
    $this->pdf->ezStartPageNumbers(560,20,10,'center','P�gina {PAGENUM} de {TOTALPAGENUM}');

    $imgDir = SF_ROOT_DIR.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
    $this->pdf->addJpegFromFile($imgDir.'europel.jpg', 500, 775, 40, 39.5);
    //$this->pdf->addJpegFromFile($imgDir.'umu.jpg', 500, 775, 40, 39.5);
    //$this->pdf->ezImage($imgDir.'umu.jpg',20,40,'none','center',0);

    $this->pdf->setColor(0.6,0.6,0.6);
    $this->pdf->filledRectangle(0,750,600,20);

    $this->salto(10);

    $con = Propel::getConnection();
    $sql = "SELECT a.nombre as nombre, p.pais as pais, sum(c.resultado) as total "
    ."FROM alumnos as a, calculos as c, equipos as e, paises as p "
    ."WHERE a.id_alumno=c.id_alumno "
    ."AND a.id_equipo=e.id_equipo "
    ."AND e.id_pais=p.id_pais "
    ."GROUP BY a.id_alumno "
    ."ORDER BY total";

    $stmt = $con->prepareStatement($sql);
    $rs = $stmt->executeQuery();

    $i = 1;
    $listado = array();
    while ($rs->next()) {
      $listado[] = array(
      'pos' => $i,
      'nombre' => utf8_decode($rs->get('nombre')),
      'pais' => utf8_decode($rs->get('pais')),
      'total' => $rs->get('total')
      );
      $i++;
    }

    for ($i=0;$i<3;$i++) {
      foreach ($listado[$i] as $key => $value) {
        $listado[$i][$key] = '<b>'.$value.'</b>';
      }
    }

    $this->pdf->ezTable($listado,
    array(
    'pos'    => '<b>N�</b>',
    'nombre' => '<b>Nombre</b>',
    'pais'   => '<b>Pa�s</b>',
    'total'  => '<b>Puntos</b>'
    ),
    '',
    array (
    'showLines'     =>  0,
    'shaded'        =>  0,
    'xPos'          =>  'center',
    'width'         =>  550,
    'fontSize'      =>  10,
    'titleFontSize' =>  13,
    'cols'          =>  array(
    'pos'    =>  array('width'=>50,'justification'=>'center'),
    'nombre'       =>  array('width'=>250),
    'pais'  =>  array('width'=>150),
    'total' =>  array('width'=>100,'justification'=>'center')
    )
    )
    );


    /**
     * Resultados por equipos
     * select id_equipo, sum(resultado) as resultado from calculos group by id_equipo order by resultado;
     * 
     */
    $this->pdf->ezNewPage();

    $this->pdf->setColor(0,0,0.5);
    $this->pdf->filledRectangle(0,770,600,50);

    $this->salto(-25);

    $this->pdf->setColor(1,1,1);
    $this->pdf->ezText('<b>'.__('Resultado por equipos').'</b>',14,array());

    $this->salto(10);

    $this->pdf->ezText($fecha,10);
    $this->pdf->setColor(0,0,0);

    $imgDir = SF_ROOT_DIR.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
    $this->pdf->addJpegFromFile($imgDir.'europel.jpg', 500, 775, 40, 39.5);
    //$this->pdf->addJpegFromFile($imgDir.'umu.jpg', 500, 775, 40, 39.5);
    //$this->pdf->ezImage($imgDir.'umu.jpg',20,40,'none','center',0);

    $this->pdf->setColor(0.6,0.6,0.6);
    $this->pdf->filledRectangle(0,750,600,20);

    $this->salto(10);

    $con = Propel::getConnection();
    $sql = "SELECT e.id_equipo as id_equipo, e.nombre as nombre, p.pais as pais, sum(resultado) as total "
    ."FROM equipos e, calculos c, paises p "
    ."WHERE e.id_pais=p.id_pais "
    ."AND e.id_equipo=c.id_equipo "
    ."GROUP BY c.id_equipo "
    ."ORDER BY total";

    $stmt = $con->prepareStatement($sql);
    $rs = $stmt->executeQuery();

    $i = 1;
    $j = 1;
    $listado = array();
    $listado2 = array();
    while ($rs->next()) {
      $c = new Criteria();
      $c->add(AlumnosPeer::ID_EQUIPO, $rs->get('id_equipo'));
      $alumnos = AlumnosPeer::doCount($c);
      
      if($alumnos==3) {
        $listado[] = array(
        'pos' => $i,
        'nombre' => utf8_decode($rs->get('nombre')),
        'pais' => utf8_decode(__($rs->get('pais'))),
        'total' => $rs->get('total')
        );
        $i++;
      } else {
        $listado2[] = array(
        'pos' => $j,
        'nombre' => utf8_decode($rs->get('nombre')),
        'pais' => utf8_decode(__($rs->get('pais'))),
        'total' => $rs->get('total')
        );
        $j++;
      }
    }

    for ($j=0;$j<count($listado2);$j++) {
      $listado2[$j]['pos'] = $i;
      $i++;
      $listado[] = $listado2[$j];
    }
    
    array_merge($listado,$listado2);
    
    for ($i=0;$i<3;$i++) {
      foreach ($listado[$i] as $key => $value) {
        $listado[$i][$key] = '<b>'.$value.'</b>';
      }
    }

    $this->pdf->ezTable($listado,
    array(
    'pos'    => '<b>'.__('N�').'</b>',
    'nombre' => '<b>'.__('Equipo').'</b>',
    'pais'   => '<b>'.__('Pa�s').'</b>',
    'total'  => '<b>'.__('Puntos').'</b>'
    ),
    '',
    array (
    'showLines'     =>  0,
    'shaded'        =>  0,
    'xPos'          =>  'center',
    'width'         =>  550,
    'fontSize'      =>  10,
    'titleFontSize' =>  13,
    'cols'          =>  array(
    'pos'    =>  array('width'=>50,'justification'=>'center'),
    'nombre'       =>  array('width'=>250),
    'pais'  =>  array('width'=>150),
    'total' =>  array('width'=>100,'justification'=>'center')
    )
    )
    );

    /**
     * Resultados por muestra
     */
    $sql = "SELECT t.id_tipomuestra as id_tipomuestra, t.nombre as t_nombre, a.nombre as a_nombre, p.pais as pais, sum(c.resultado) as total "
    ."FROM tipomuestra t, alumnos a, calculos c, equipos e, paises p "
    ."WHERE t.id_tipomuestra=c.id_tipo and a.id_alumno=c.id_alumno and a.id_equipo=e.id_equipo and e.id_pais=p.id_pais "
    ."GROUP BY c.id_alumno,c.id_tipo "
    ."ORDER BY id_tipomuestra, total";

    $stmt = $con->prepareStatement($sql);
    $rs = $stmt->executeQuery();

    $nombre_muestra = '';
    $id_tipomuestra = 0;
    $listado = array();
    while ($rs->next()) {
      if ($rs->get('id_tipomuestra')!=$id_tipomuestra) {
        if($id_tipomuestra>0) {
          $this->pdf->ezNewPage();

          $this->pdf->setColor(0,0,0.5);
          $this->pdf->filledRectangle(0,770,600,50);

          $this->salto(-25);

          $this->pdf->setColor(1,1,1);
          $this->pdf->ezText('<b>'.__('Resultado muestras de %%nombre%%',array('%%nombre%%' => $nombre_muestra)).'</b>',14,array());

          $this->salto(10);

          $this->pdf->ezText($fecha,10);
          $this->pdf->setColor(0,0,0);

          $imgDir = SF_ROOT_DIR.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
          $this->pdf->addJpegFromFile($imgDir.'europel.jpg', 500, 775, 40, 39.5);
          //$this->pdf->addJpegFromFile($imgDir.'umu.jpg', 500, 775, 40, 39.5);
          //$this->pdf->ezImage($imgDir.'umu.jpg',20,40,'none','center',0);

          $this->pdf->setColor(0.6,0.6,0.6);
          $this->pdf->filledRectangle(0,750,600,20);

          $this->salto(10);

          for ($i=0;$i<3;$i++) {
            foreach ($listado[$i] as $key => $value) {
              $listado[$i][$key] = '<b>'.$value.'</b>';
            }
          }
          
          $this->pdf->ezTable($listado,
          array(
          'pos'    => '<b>'.__('N�').'</b>',
          'nombre' => '<b>'.__('Nombre').'</b>',
          'pais'   => '<b>'.__('Pa�s').'</b>',
          'total'  => '<b>'.__('Puntos').'</b>'
          ),
          '',
          array (
          'showLines'     =>  0,
          'shaded'        =>  0,
          'xPos'          =>  'center',
          'width'         =>  550,
          'fontSize'      =>  10,
          'titleFontSize' =>  13,
          'cols'          =>  array(
          'pos'    =>  array('width'=>50,'justification'=>'center'),
          'nombre'       =>  array('width'=>250),
          'pais'  =>  array('width'=>150),
          'total' =>  array('width'=>100,'justification'=>'center')
          )
          )
          );
        }
        $nombre_muestra = $rs->get('t_nombre');
        $id_tipomuestra = $rs->get('id_tipomuestra');
        $listado = array();
        $i = 1;
      }
      $listado[] = array(
      'pos' => $i,
      'nombre' => utf8_decode($rs->get('a_nombre')),
      'pais' => utf8_decode(__($rs->get('pais'))),
      'total' => $rs->get('total')
      );
      $i++;
    }
    $this->pdf->ezNewPage();

    $this->pdf->setColor(0,0,0.5);
    $this->pdf->filledRectangle(0,770,600,50);

    $this->salto(-25);

    $this->pdf->setColor(1,1,1);
    $this->pdf->ezText('<b>'.__('Resultado muestras de %%nombre%%',array('%%nombre%%' => $nombre_muestra)).'</b>',14,array());

    $this->salto(10);

    $this->pdf->ezText($fecha,10);
    $this->pdf->setColor(0,0,0);

    $imgDir = SF_ROOT_DIR.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
    $this->pdf->addJpegFromFile($imgDir.'europel.jpg', 500, 775, 40, 39.5);
    //$this->pdf->addJpegFromFile($imgDir.'umu.jpg', 500, 775, 40, 39.5);
    //$this->pdf->ezImage($imgDir.'umu.jpg',20,40,'none','center',0);

    $this->pdf->setColor(0.6,0.6,0.6);
    $this->pdf->filledRectangle(0,750,600,20);

    $this->salto(10);

    for ($i=0;$i<3;$i++) {
      foreach ($listado[$i] as $key => $value) {
        $listado[$i][$key] = '<b>'.$value.'</b>';
      }
    }
    
    $this->pdf->ezTable($listado,
    array(
    'pos'    => '<b>'.__('N�').'</b>',
    'nombre' => '<b>'.__('Nombre').'</b>',
    'pais'   => '<b>'.__('Pa�s').'</b>',
    'total'  => '<b>'.__('Puntos').'</b>'
    ),
    '',
    array (
    'showLines'     =>  0,
    'shaded'        =>  0,
    'xPos'          =>  'center',
    'width'         =>  550,
    'fontSize'      =>  10,
    'titleFontSize' =>  13,
    'cols'          =>  array(
    'pos'    =>  array('width'=>50,'justification'=>'center'),
    'nombre'       =>  array('width'=>250),
    'pais'  =>  array('width'=>150),
    'total' =>  array('width'=>100,'justification'=>'center')
    )
    )
    );


    /**
     * STREAMING del PDF
     */
    return $this->pdf->ezOutput('','S');
  }

  private function getMes($mes) {
    switch ($mes) {
      case 1: return 'Enero';
      case 2: return 'Febrero';
      case 3: return 'Marzo';
      case 4: return 'Abril';
      case 5: return 'Mayo';
      case 6: return 'Junio';
      case 7: return 'Julio';
      case 8: return 'Agost';
      case 9: return 'Septiembre';
      case 10: return 'Octubre';
      case 11: return 'Noviembre';
      case 12: return 'Diciembre';
      default: return '';
    }
  }

  /**
   * M�todos privados de la clase
   *
   */

  /**
   * La funci�n salto($step) nos permite dar un salto en el folio del PDF
   *
   * @param int $step
   */
  private function salto($step=10) {
    $this->pdf->y = $this->pdf->y - $step;
  }

}
